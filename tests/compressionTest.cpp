#include "../headers/compression.h"
#include "../headers/fileio.h"
#include <iostream>
#include <unistd.h>

int main()
{
    mkdir("testdir", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    std::string dir = "testdir";
    FileIO::write_file("testdir/file.txt","testing text");

    if(FileIO::dir_exists(dir))
    {
        FileIO f_obj = FileIO();
        std::vector<std::string> files = f_obj.path_list(dir);
        Compression::create("output.tar.bz2",files);

        if(!FileIO::file_exists("output.tar.bz2"))
        {
            std::cerr << "Could not find compressed tarball" << std::endl;
            return -1;
        }

        Compression::extract("output.tar.bz2", "output/");
        if(!FileIO::dir_exists("output"))
        {
            std::cerr << "Could not find the extraction directory" << std::endl;
            return -1;
        }

        std::string contents = FileIO::read_file("output/testdir/file.txt");
        if(contents != "testing text\n")
        {
            std::cerr << "The extracted files did not match the created files." << std::endl;
            return -1;
        }

        FileIO obj = FileIO();

        if(!obj.delete_directory("output"))
        {
            std::cerr << "Could not delete the extraction directory" << std::endl;
            return -1;
        }

        if(!obj.delete_directory("testdir"))
        {
            std::cerr << "Could not delete the test directory" << std::endl;
            return -1;
        }

        if(!FileIO::delete_file("output.tar.bz2"))
        {
            std::cerr << "Could not delete the tarball" << std::endl;
            return -1;
        }
    }else{
        std::cerr << "Could not find the target directory" << std::endl;
        return -1;
    }

}
