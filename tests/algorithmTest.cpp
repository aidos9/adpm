#include "../headers/algorithms.h"
#include <vector>
#include <string>
#include <iostream>

#define BINARY_TARGET_1 "Roof"
#define BINARY_TARGET_2 "Adult"
#define BINARY_TARGET_3 "Adult"
#define VALUES_TARGET_1 "Air"

int main()
{
    std::vector<std::string> unordered = {"Roof", "Window", "Aircraft Carrier","Girl", "Airforce",
                                          "Airport", "Church", "Album", "Mosquito", "Alphabet","Electricity", "Apple", "Bible", "Butterfly",
                                          "Cup","Tapestry",  "Air", "Floodlight", "Ice-cream", "Man",
                                          "X-ray", "Printer", "Adult",  "Shop", "Spoon",  "Triangle",
                                           "Passport","Aeroplane", "Bee"};
    const std::vector<std::string> comparison = {"Adult", "Aeroplane", "Air", "Aircraft Carrier", "Airforce",
                                           "Airport","Album", "Alphabet", "Apple", "Bee", "Bible", "Butterfly", "Church",
                                           "Cup", "Electricity", "Floodlight", "Girl", "Ice-cream", "Man", "Mosquito",
                                           "Passport", "Printer", "Roof", "Shop", "Spoon", "Tapestry", "Triangle",
                                           "Window", "X-ray"};
    const std::vector<std::string> expected_search_1 = {"Air", "Aircraft Carrier", "Airforce", "Airport"};

    Algorithms::alpha_order_vector(unordered);
    if(unordered != comparison)
    {
        std::cerr << "Failed to successfully order the vector" << std::endl;
        return -1;
    }

    if(comparison[Algorithms::binary_search(comparison, BINARY_TARGET_1)] != BINARY_TARGET_1)
    {
        std::cerr << "Binary search failed to find the target value " << BINARY_TARGET_1 << std::endl;
        return -1;
    }

    if(comparison[Algorithms::binary_search(comparison, BINARY_TARGET_2)] != BINARY_TARGET_2)
    {
        std::cerr << "Binary search failed to find the target value " << BINARY_TARGET_2 << std::endl;
        return -1;
    }

    if(comparison[Algorithms::binary_search(comparison, BINARY_TARGET_3)] != BINARY_TARGET_3)
    {
        std::cerr << "Binary search failed to find the target value " << BINARY_TARGET_3 << std::endl;
        return -1;
    }

    if(Algorithms::random_string(100).size() != 100)
    {
        std::cerr << "Random string didn't generate the correct size string" << std::endl;
        return -1;
    }

    if(Algorithms::values_containing_substr(comparison, VALUES_TARGET_1) != expected_search_1)
    {
        std::cerr << "values_containing_substr didn't return exepcted values" << std::endl;
        return -1;
    }

    if(Algorithms::values_containing_substr(comparison, "nullstring").size() != 0)
    {
        std::cerr << "values_containing_substr didn't return exepcted values" << std::endl;
        return -1;
    }
	
	std::cout << "Success" << std::endl;
	return 0;
}
