#include "../headers/network.h"
#include "../headers/fileio.h"
#include <iostream>

#define URL "https://raw.githubusercontent.com/aidos9/adpm-repo/master/index.adpm"
#define EXPECTED_FILENAME "index.adpm"

int main()
{
    Network net = Network();
    std::string filename;

    if(!net.get_filename_from_url(URL,filename))
    {
        std::cerr << "Could not get expected filename" << std::endl;
        return -1;
    }

    if(filename != EXPECTED_FILENAME)
    {
        std::cerr << "Could not get expected filename" << std::endl;
        return -1;
    }

    if(!net.download(URL, true))
    {
        std::cerr << "Could not get download file" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file(filename))
    {
        std::cerr << "Could not get delete file" << std::endl;
        return -1;
    }

    if(!net.download_filename(URL, true, "download"))
    {
        std::cerr << "Could not get download file" << std::endl;
        return -1;
    }

    if(!FileIO::file_exists("download"))
    {
        std::cerr << "Could not detect downloaded file" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("download"))
    {
        std::cerr << "Could not get delete file" << std::endl;
        return -1;
    }
    return 0;
}
