#include "../headers/fileio.h"
#include "../headers/database.h"
#include "../headers/package.h"
#include "../headers/configfile.h"
#include "../headers/packageinfo.h"
#ifdef WITH_NETWORK
#include "../headers/index.h"
#endif
#include <iostream>

int main()
{
    //Check package
    Package pack = Package();
    pack.build_commands = {"build_command"};
    pack.description = "package-desc";
    pack.version = "package-ver";
    pack.name = "package-name";
    pack.runtime_dependencies = {"runtime_depend"};
    pack.make_dependencies = {"make_depend"};
    pack.package_commands = {"package_command"};
    pack.prepare_commands = {"prepare_command"};
    pack.sources = {"https://test.com/test.adpm"};
    pack.variables = {{"threads", "4"}};
    pack.with_sources_arg = true;
    pack.with_variables_arg = true;
    pack.with_clean = true;
    pack.with_build_dependencies_arg = true;
    pack.with_dependencies_arg = true;
    pack.cleanup_files = {"extracted_directory"};
    pack.with_test = true;
    pack.test_commands = {"make test"};

    if(!FileIO::write_package("package-test.adpm", pack))
    {
        std::cerr << "Failed to write package file" << std::endl;
        return -1;
    }

    Package read;
    if(!FileIO::read_package("package-test.adpm", read))
    {
        std::cerr << "Failed to read package file" << std::endl;
        return -1;
    }

    if(read != pack)
    {
        std::cerr << "Written package file does not match the read one" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("package-test.adpm"))
    {
        std::cerr << "Failed to delete package" << std::endl;
        return -1;
    }

    //Check database
    Database db = Database();
    db.index_files = {{"file1","file"}};
    db.package_files = {{"file1",{"file"}}};
    db.package_info = {{"file2", "fileinfo"}};
    db.package_list = {"package"};
    db.package_version = {{"package","v1.0"}};

    if(!FileIO::write_database("db-test", db))
    {
        std::cerr << "Failed to write database file" << std::endl;
        return -1;
    }

    Database d_read;
    if(!FileIO::read_database("db-test", d_read))
    {
        std::cerr << "Failed to read database file" << std::endl;
        return -1;
    }

    if(db != d_read)
    {
        std::cerr << "Written database file does not match the read one" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("db-test"))
    {
        std::cerr << "Failed to delete database" << std::endl;
        return -1;
    }

    //Check config
    Config conf = Config();
    conf.db_path = "dumby";
    conf.remote_indexes = {"https://fake.com"};
    conf.variables = {{"var","val"}};
    conf.with_color = true;

    if(!FileIO::write_config("config-test", conf))
    {
        std::cerr << "Failed to write config file" << std::endl;
        return -1;
    }

    Config conf_read;
    if(!FileIO::read_config("config-test", conf_read))
    {
        std::cerr << "Failed to read config file" << std::endl;
        return -1;
    }

    if(conf != conf_read)
    {
        std::cerr << "Written config file does not match the read one" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("config-test"))
    {
        std::cerr << "Failed to delete config" << std::endl;
        return -1;
    }

#ifdef WITH_NETWORK
    //Check index
    Index ind = Index();
    ind.packages = {"pkg1"};
    ind.urls = {{"pkg1", "https://web.com"}};
    ind.versions = {{"pkg1", "v1.0"}};

    if(!FileIO::write_index("index-test", ind))
    {
        std::cerr << "Failed to write index file" << std::endl;
        return -1;
    }

    Index ind_read;
    if(!FileIO::read_index("index-test", ind_read))
    {
        std::cerr << "Failed to read index file" << std::endl;
        return -1;
    }

    if(ind != ind_read)
    {
        std::cerr << "Written index file does not match the read one" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("index-test"))
    {
        std::cerr << "Failed to delete index" << std::endl;
        return -1;
    }
#endif

    //Check Packageinfo
    PackageInfo info = PackageInfo();
    info.files = {"downloads/test", "bob/test"};
    info.description = "Test package, for testing the parsing";
    info.name = "test";
    info.version = "0.0.0";

    if(!FileIO::write_pkginfo("pkginfo-test", info))
    {
        std::cerr << "Failed to write package info file" << std::endl;
        return -1;
    }

    PackageInfo info_read;
    if(!FileIO::read_pkginfo("pkginfo-test", info_read))
    {
        std::cerr << "Failed to read package info file" << std::endl;
        return -1;
    }

    if(info != info_read)
    {
        std::cerr << "Written package info file does not match the read one" << std::endl;
        return -1;
    }

    if(!FileIO::delete_file("pkginfo-test"))
    {
        std::cerr << "Failed to delete package info file" << std::endl;
        return -1;
    }

    std::cout << "Parse success" << std::endl;
	return 0;
}
