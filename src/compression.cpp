#include "../headers/compression.h"
#include "../headers/fileio.h"
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>

Compression::Compression()
{

}

void Compression::create(const std::string filename, const std::vector<std::string> files)
{
    struct archive *a;
    struct archive_entry *entry;
    struct stat st;
    char buff[8192];
    int len;
    int fd;

    a = archive_write_new();
    archive_write_add_filter_gzip(a);
    archive_write_set_format_pax(a);
    archive_write_open_filename(a, filename.c_str());

    for (int i = 0; i < files.size(); i++) {
        stat(files[i].c_str(), &st);

        fflush(stdout);
        std::cout << "Progress: " << i << "/" << files.size() << "\r";

        entry = archive_entry_new();

        archive_entry_set_pathname(entry, files[i].c_str());
        archive_entry_copy_stat(entry, &st);
        archive_write_header(a, entry);

        fd = open(files[i].c_str(), O_RDONLY);
        len = read(fd, buff, sizeof(buff));

        while ( len > 0 ) {
            archive_write_data(a, buff, len);
            len = read(fd, buff, sizeof(buff));
        }

        close(fd);
        archive_entry_free(entry);
    }
    archive_write_close(a);
    archive_write_free(a);
}

void Compression::extract(const std::string filename)
{
    extract(filename, "");
}

void Compression::extract(const std::string filename, const std::string target)
{
    struct archive *a;
    struct archive *ext;
    struct archive_entry *entry;
    int flags;
    int r;

    flags = ARCHIVE_EXTRACT_TIME | ARCHIVE_EXTRACT_PERM | ARCHIVE_EXTRACT_ACL | ARCHIVE_EXTRACT_FFLAGS;

    a = archive_read_new();
    archive_read_support_format_all(a);
    archive_read_support_filter_all(a);
    ext = archive_write_disk_new();
    archive_write_disk_set_options(ext, flags);
    archive_write_disk_set_standard_lookup(ext);

    if ((r = archive_read_open_filename(a, filename.c_str(), 10240)))
    {
        return;
    }

    for (;;) {
        r = archive_read_next_header(a, &entry);

        if (r == ARCHIVE_EOF)
        {
            break;
        }

        if (r < ARCHIVE_OK)
        {
            fprintf(stderr, "%s\n", archive_error_string(a));
        }

        if (r < ARCHIVE_WARN)
        {
            return;
        }

        std::string path = target;
        path += archive_entry_pathname(entry);
        archive_entry_set_pathname(entry, path.c_str());
        r = archive_write_header(ext, entry); //Writes the file

        if (r < ARCHIVE_OK)
        {
            fprintf(stderr, "%s\n", archive_error_string(ext));
        }else if (archive_entry_size(entry) > 0) {
            r = copy_data(a, ext);

            if (r < ARCHIVE_OK)
            {
                fprintf(stderr, "%s\n", archive_error_string(ext));
            }

            if (r < ARCHIVE_WARN)
            {
                return;
            }
        }
        r = archive_write_finish_entry(ext);

        if (r < ARCHIVE_OK)
        {
            fprintf(stderr, "%s\n", archive_error_string(ext));
        }

        if (r < ARCHIVE_WARN)
        {
            return;
        }
    }

    archive_read_close(a);
    archive_read_free(a);
    archive_write_close(ext);
    archive_write_free(ext);
}

int Compression::copy_data(struct archive *ar, struct archive *aw)
{
    int r;
    const void *buff;
    size_t size;
    la_int64_t offset;

    for(;;)
    {
        r = archive_read_data_block(ar, &buff, &size, &offset);
        if (r == ARCHIVE_EOF)
        {
            return ARCHIVE_OK;
        }

        if(r < ARCHIVE_OK)
        {
            return r;
        }

        r = archive_write_data_block(aw, buff, size, offset);

        if(r < ARCHIVE_OK)
        {
            std::cerr << "decompression error: " << archive_error_string(aw) << std::endl;
            return r;
        }
    }
}
