#include "../headers/fileio.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <unordered_map>

FileIO::FileIO()
{

}

static bool success = true;

bool FileIO::file_exists(const std::string path)
{
    struct stat buffer;
    return (stat(path.c_str(), &buffer) == 0);
}

bool FileIO::dir_exists(const std::string path)
{
    struct stat buffer;
    int dir = stat(path.c_str(), &buffer);
    return(dir == 0 && S_ISDIR(buffer.st_mode));
}

std::string FileIO::read_file(const std::string path)
{
    std::ifstream input_stream;
    input_stream.open(path, std::ios::in);
    std::string return_val = "";

    if(input_stream.is_open())
    {
        std::string line;
        while(getline(input_stream,line))
        {
            return_val.append(line);
            return_val.append("\n");
        }
    }else{
        perror("adpm");
        input_stream.close();
        return "";
    }

    input_stream.close();
    return return_val;
}

bool FileIO::write_file(const std::string path, const std::string contents)
{
    std::ofstream output_stream;
    output_stream.open(path,std::ios::out);

    if(output_stream.is_open())
    {
        output_stream << contents;
    }else{
        perror("adpm");
        output_stream.close();
        return false;
    }

    output_stream.close();
    return true;
}

bool FileIO::read_json(const std::string path, nlohmann::json &js)
{
    std::string contents = read_file(path);
    if(contents != "")
    {
        try{
            js = nlohmann::json::parse(contents);
        }catch(nlohmann::json::exception& e)
        {
            std::cerr << "Error with JSON: " << e.what() << std::endl;
            return false;
        }
    }else{
        js = {};
    }

    return true;
}

bool FileIO::write_json(const std::string path, const nlohmann::json &js)
{
    try{
        bool ret = write_file(path,js.dump(4));
        return ret;
    }catch(nlohmann::json::exception& e)
    {
        std::cerr << "Error with JSON: " << e.what() << std::endl;
        return false;
    }
}

bool FileIO::read_package(const std::string path, Package &package)
{
    nlohmann::json js;
    if(!read_json(path,js))
    {
        return false;
    }

    try{
        package = js.get<Package>();
    }catch(nlohmann::json::exception& e)
    {
        std::cerr << "Error with JSON: " << e.what() << std::endl;
        return false;
    }

    return success;
}

bool FileIO::write_package(const std::string path, const Package &pack)
{
    nlohmann::json js = pack;
    if(js == NULL)
    {
        std::cerr << "JSON was equal to null" << std::endl;
        return false;
    }
    return write_json(path,js);
}

bool FileIO::write_config(const std::string path, const Config &conf)
{
    nlohmann::json js = conf;
    if(js == NULL)
    {
        std::cerr << "JSON was equal to null" << std::endl;
        return false;
    }
    return write_json(path,js);
}

bool FileIO::read_database(const std::string path, Database &db)
{
    nlohmann::json js;
    if(!read_json(path,js))
    {
        return false;
    }

    try{
        db = js.get<Database>();
    }catch(nlohmann::json::exception& e)
    {
        std::cerr << "Error with JSON: " << e.what() << std::endl;
        return false;
    }

    return success;
}

bool FileIO::write_database(const std::string path, const Database &db)
{
    nlohmann::json js = db;
    if(js == NULL)
    {
        std::cerr << "JSON was equal to null" << std::endl;
        return false;
    }
    return write_json(path,js);
}
#ifdef WITH_NETWORK
bool FileIO::read_index(const std::string path, Index &index)
{
    nlohmann::json js;
    if(!read_json(path,js))
    {
        return false;
    }

    try{
        index = js.get<Index>();
    }catch(nlohmann::json::exception& e)
    {
        std::cerr << "Error with JSON: " << e.what() << std::endl;
        return false;
    }

    return success;
}

bool FileIO::write_index(const std::string path, const Index &index)
{
    nlohmann::json js = index;
    if(js == NULL)
    {
        std::cout << "JSON was equal to null" << std::endl;
        return false;
    }
    return write_json(path,js);
}
#endif

bool FileIO::read_pkginfo(const std::string path, PackageInfo &info)
{
    nlohmann::json js;
    if(!read_json(path, js))
    {
        return false;
    }

    try {
        info = js.get<PackageInfo>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Exception thrown when parsing config: " << e.what() << std::endl;
        return false;
    }

    return success;
}

bool FileIO::write_pkginfo(const std::string path, const PackageInfo &info)
{
    nlohmann::json js = info;
    if(js == NULL)
    {
        std::cout << "JSON was equal to null" << std::endl;
        return false;
    }
    return write_json(path,js);
}

bool FileIO::read_config(const std::string path, Config &conf)
{
    nlohmann::json js;
    if(!read_json(path, js))
    {
        return false;
    }

    try {
        conf = js.get<Config>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Exception thrown when parsing config: " << e.what() << std::endl;
        return false;
    }
    return success;
}

std::vector<std::string> FileIO::cwd_paths()
{
    update_paths();
    return paths;
}

std::vector<std::string> FileIO::path_list(const std::string root_dir)
{
    if(!FileIO::dir_exists(root_dir))
    {
        return std::vector<std::string>();
    }

    paths.clear();
    path = root_dir;
    ProcessDir("");
    return paths;
}

std::vector<std::string> FileIO::path_list_dir(const std::string root_dir)
{
    if(!FileIO::dir_exists(root_dir))
    {
        return std::vector<std::string>();
    }

    paths.clear();
    path = root_dir;
    ProcessDir_keep_dir("");
    return paths;
}

void FileIO::update_paths()
{
    paths.clear();
    cwd(path);
    ProcessDir("");
}

void FileIO::ProcessDir(std::string dir)
{
    std::string dirToOpen = path + dir;
    auto dirp = opendir(dirToOpen.c_str());
    path = dirToOpen + "/";

    if(NULL == dirp)
    {
        std::cout << "Could not open directory: " << dir << std::endl;
        return;
    }

    auto entity = readdir(dirp);

    while(entity != NULL)
    {
        ProcessEntity(entity);
        entity = readdir(dirp);
    }

    path.resize(path.length() - 1 - dir.length());
    closedir(dirp);
}

void FileIO::ProcessDir_keep_dir(std::string dir)
{
    std::string dirToOpen = path + dir;
    auto dirp = opendir(dirToOpen.c_str());
    path = dirToOpen + "/";

    if(NULL == dirp)
    {
        std::cout << "Could not open directory: " << dir << std::endl;
        return;
    }

    auto entity = readdir(dirp);

    while(entity != NULL)
    {
        ProcessEntity_keep_dir(entity);
        entity = readdir(dirp);
    }
    paths.push_back(path);
    path.resize(path.length() - 1 - dir.length());
    closedir(dirp);
}

void FileIO::ProcessEntity_keep_dir(dirent *entity)
{
    std::string name = entity->d_name;
    if(entity->d_type == DT_DIR)
    {
        if(name == ".")
        {
            return;
        }else if(name == "..")
        {
            return;
        }

        ProcessDir_keep_dir(name);
        return;
    }

    ProcessFile(name);

    return;
}

void FileIO::ProcessEntity(dirent *entity)
{
    std::string name = entity->d_name;
    if(entity->d_type == DT_DIR)
    {
        if(name == ".")
        {
            return;
        }else if(name == "..")
        {
            return;
        }

        ProcessDir(name);
        return;
    }

    ProcessFile(name);

    return;
}

void FileIO::ProcessFile(std::string pth)
{
    paths.push_back(path + pth);
}

bool FileIO::cwd(std::string& path)
  {
    typedef std::pair<dev_t, ino_t> file_id;

    bool success = false;
    int start_fd = open(".", O_RDONLY); //Keep track of start directory, so can jump back to it later
    if (start_fd != -1)
    {
      struct stat sb;
      if (!fstat(start_fd, &sb))
      {
        file_id current_id(sb.st_dev, sb.st_ino);
        if (!stat("/", &sb)) //Get info for root directory, so we can determine when we hit it
        {
          std::vector<std::string> path_components;
          file_id root_id(sb.st_dev, sb.st_ino);

          while (current_id != root_id) //If they're equal, we've obtained enough info to build the path
          {
            bool pushed = false;

            if (!chdir("..")) //Keep recursing towards root each iteration
            {
              DIR *dir = opendir(".");
              if (dir)
              {
                dirent *entry;
                while ((entry = readdir(dir))) //We loop through each entry trying to find where we came from
                {
                  if ((strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..") && !lstat(entry->d_name, &sb)))
                  {
                    file_id child_id(sb.st_dev, sb.st_ino);
                    if (child_id == current_id) //We found where we came from, add its name to the list
                    {
                      path_components.push_back(entry->d_name);
                      pushed = true;
                      break;
                    }
                  }
                }
                closedir(dir);

                if (pushed && !stat(".", &sb)) //If we have a reason to contiue, we update the current dir id
                {
                  current_id = file_id(sb.st_dev, sb.st_ino);
                }
              }//Else, Uh oh, can't read information at this level
            }
            if (!pushed) { break; } //If we didn't obtain any info this pass, no reason to continue
          }

          if (current_id == root_id) //Unless they're equal, we failed above
          {
            //Built the path, will always end with a slash
            //path = "/";
            for (std::vector<std::string>::reverse_iterator i = path_components.rbegin(); i != path_components.rend(); ++i)
            {
              path += "/"+*i;
            }
            success = true;
          }
          fchdir(start_fd);
        }
      }
      close(start_fd);
    }

    return(success);
  }

bool FileIO::copy_file(const std::string src, const std::string dest)
{    
    std::ifstream src_stream(src, std::ios::binary);
    std::ofstream dest_stream(dest, std::ios::binary);

    if(!src_stream.is_open())
    {
        src_stream.close();
        dest_stream.close();
        perror("adpm");
        return false;
    }

    if(!dest_stream.is_open())
    {
        src_stream.close();
        dest_stream.close();
        perror("adpm");
        return false;
    }

    dest_stream << src_stream.rdbuf();

    src_stream.close();
    dest_stream.close();

    FILE * s_file;
    s_file = fopen(src.c_str(), "r+");

    if(s_file == NULL)
    {
        perror("adpm");
        return false;
    }

    struct stat s_buff;

    if(fstat(fileno(s_file), &s_buff) != 0)
    {
        perror("adpm");
        fclose(s_file);
        return false;
    }

    if(chmod(dest.c_str(), s_buff.st_mode) != 0)
    {
        perror("adpm");
        fclose(s_file);
        return false;
    }

    fclose(s_file);

    return true;
}

bool FileIO::delete_file(const std::string& path)
{
    if(remove(path.c_str()) != 0 && errno != ENOENT)
    {
        perror("adpm");
        return false;
    }else{
        return true;
    }
}

bool FileIO::delete_directory(const std::string& path)
{
    std::vector<std::string> file_list = path_list_dir(path);
    for(auto file : file_list)
    {
        if(!delete_file(file))
        {
            return false;
        }
    }

    return delete_file(path);
}

bool FileIO::delete_misc(const std::string& path)
{
    struct stat buffer;
    int dir = stat(path.c_str(), &buffer);

    if(dir != 0)
    {
        return true;
    }

    if(S_ISDIR(buffer.st_mode))
    {
        return delete_directory(path);
    }else if(S_ISREG(buffer.st_mode))
    {
         return delete_file(path);
    }else{
        std::cerr << "Unknown file trying to be deleted" << std::endl;
        return false;
    }
}

bool FileIO::is_dir(const std::string& path)
{
    struct stat buffer;
    if(stat(path.c_str(), &buffer) != 0)
    {
        return false;
    }
    return (S_ISDIR(buffer.st_mode) == 0);
}

void to_json(nlohmann::json &j, const Database &db)
{
    j = db.to_json();
}

void from_json(const nlohmann::json &j, Database &db)
{
    success = db.from_json(j);
}

void to_json(nlohmann::json &j, const PackageInfo &pkginfo)
{
    j = pkginfo.to_json();
}

void from_json(const nlohmann::json &j, PackageInfo &pkginfo)
{
    success = pkginfo.from_json(j);
}

void to_json(nlohmann::json &j, const Package &p)
{
    j = p.to_json();
}

void from_json(const nlohmann::json &j, Package &p)
{
    success = p.from_json(j);
}

#ifdef WITH_NETWORK
void to_json(nlohmann::json &j, const Index &index)
{
    j = index.to_json();
}

void from_json(const nlohmann::json &j, Index &index)
{
    success = index.from_json(j);
}
#endif

void to_json(nlohmann::json &j, const Config &conf)
{
    j = conf.to_json();
}

void from_json(const nlohmann::json &j, Config &conf)
{
    success = conf.from_json(j);
}
