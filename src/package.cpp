#include "../headers/package.h"
#include <iostream>

Package::Package()
{

}

bool Package::is_valid()
{
    bool valid = true;

    if(name.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No name was provided" << std::endl;
        valid = false;
    }

    if(version.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No version was provided" << std::endl;
        valid = false;
    }

    if(description.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No description was provided" << std::endl;
        valid = false;
    }

    if(build_commands.size() == 0)
    {
        std::cerr << "No build commands were provided" << std::endl;
        valid = false;
    }

    if(package_commands.size() == 0)
    {
        std::cerr << "No package commands were provided" << std::endl;
        valid = false;
    }

    return valid;
}

bool Package::from_json(const nlohmann::json &j)
{
    if(j.find("test") != j.end() && j.at("test").is_array())
    {
        test_commands = j.at("test").get<std::vector<std::string>>();
        with_test = true;
    }else{
        with_test = false;
    }

    if(j.find("clean_files") != j.end() && j.at("clean_files").is_array())
    {
        cleanup_files = j.at("clean_files").get<std::vector<std::string>>();
        with_clean = true;
    }else{
        with_clean = false;
    }

    if(j.find("sources") != j.end() && j.at("sources").is_array())
    {
        sources = j.at("sources").get<std::vector<std::string>>();
        with_sources_arg = true;
    }else{
        with_sources_arg = false;
    }

    if(j.find("variables") == j.end())
    {
        with_variables_arg = false;
    }else{
        try {
            variables = j.at("variables").get<std::unordered_map<std::string, std::string>>();
            with_variables_arg = true;
        } catch (nlohmann::json::exception &e) {
            std::cerr << "Exception thrown when parsing json: " << e.what() << std::endl;
            return false;
        }
    }

    if(j.find("dependencies") == j.end())
    {
        with_dependencies_arg = false;
    }else{
        try {
            runtime_dependencies = j.at("dependencies").get<std::vector<std::string>>();
            with_dependencies_arg = true;
        } catch (nlohmann::json::exception &e) {
            std::cerr << "Exception thrown when parsing json: " << e.what() << std::endl;
            return false;
        }
    }

    if(j.find("build_dependencies") == j.end())
    {
        with_build_dependencies_arg = false;
    }else{
        try {
            make_dependencies = j.at("build_dependencies").get<std::vector<std::string>>();
            with_build_dependencies_arg = true;
        } catch (nlohmann::json::exception &e) {
            std::cerr << "Exception thrown when parsing json: " << e.what() << std::endl;
            return false;
        }
    }

    if(j.find("package_name") == j.end() || !j.at("package_name").is_string())
    {
        std::cerr << "Invalid package_name. Please set a string." << std::endl;
        return false;
    }else if(j.find("package_version") == j.end() || !j.at("package_version").is_string())
    {
        std::cerr << "Invalid package_version. Please set a string." << std::endl;
        return false;
    }else if(j.find("package_desc") == j.end() || !j.at("package_desc").is_string())
    {
        std::cerr << "Invalid package_desc. Please set a string." << std::endl;
        return false;
    }else if(j.find("prepare") == j.end() || !j.at("prepare").is_array())
    {
        std::cerr << "Invalid prepare. Please set an array of strings or provide an empty array." << std::endl;
        return false;
    }else if(j.find("build") == j.end() || !j.at("build").is_array())
    {
        std::cerr << "Invalid build. Please set an array of strings or provide an empty array." << std::endl;
        return false;
    }else if(j.find("package") == j.end() || !j.at("package").is_array())
    {
        std::cerr << "Invalid package. Please set an array of strings or provide an empty array." << std::endl;
        return false;
    }

    try {
        name = j.at("package_name").get<std::string>();
        version = j.at("package_version").get<std::string>();
        description = j.at("package_desc").get<std::string>();
        prepare_commands = j.at("prepare").get<std::vector<std::string>>();
        build_commands = j.at("build").get<std::vector<std::string>>();
        package_commands = j.at("package").get<std::vector<std::string>>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Exception thrown when parsing json: " << e.what() << std::endl;
        return false;
    }

    return true;
}

nlohmann::json Package::to_json() const
{
    nlohmann::json j = {{"package_name",name},{"package_version",version},{"package_desc",description},
         {"prepare",prepare_commands},{"build",build_commands}, {"package",package_commands}};

    if(with_build_dependencies_arg)
    {
        j["build_dependencies"] = make_dependencies;
    }

    if(with_dependencies_arg)
    {
        j["dependencies"] = runtime_dependencies;
    }

    if(with_sources_arg)
    {
        j["sources"] = sources;
    }

    if(with_variables_arg)
    {
        j["variables"] = variables;
    }

    if(with_clean)
    {
        j["clean_files"] = cleanup_files;
    }

    if(with_test)
    {
        j["test"] = test_commands;
    }

    return j;
}

bool Package::operator==(const Package &other)
{
    return (other.description == description && other.version == version && other.name == name && other.build_commands == build_commands
            && other.make_dependencies == make_dependencies && other.package_commands == package_commands
            && other.prepare_commands == prepare_commands && other.runtime_dependencies == runtime_dependencies
            && other.sources == sources && other.variables == variables && other.variables_parsed == variables_parsed
            && other.with_sources_arg == with_sources_arg && other.with_variables_arg == with_variables_arg
            && other.test_commands == test_commands && other.with_test == with_test
            && other.with_clean == with_clean && other.cleanup_files == cleanup_files
            && other.with_build_dependencies_arg == with_build_dependencies_arg && other.with_dependencies_arg == with_dependencies_arg);
}

bool Package::operator!=(const Package &other)
{
    return (other.description != description || other.version != version || other.name != name || other.build_commands != build_commands
            || other.make_dependencies != make_dependencies || other.package_commands != package_commands
            || other.prepare_commands != prepare_commands || other.runtime_dependencies != runtime_dependencies
            || other.sources != sources || other.variables != variables || other.variables_parsed != variables_parsed
            || other.with_sources_arg != with_sources_arg || other.with_variables_arg != with_variables_arg
            || other.with_clean != with_clean || other.cleanup_files != cleanup_files
            || other.with_test != with_test || other.test_commands != test_commands
            || other.with_build_dependencies_arg != with_build_dependencies_arg || other.with_dependencies_arg != with_dependencies_arg);
}
