#include "../headers/index.h"
#include <iostream>

Index::Index()
{

}

nlohmann::json Index::to_json() const
{
    nlohmann::json j = {{"packages", packages},{"urls", urls},{"versions",versions}};
    return j;
}

bool Index::from_json(const nlohmann::json &j)
{
    if(j.find("packages") == j.end() || !j.at("packages").is_array())
    {
        std::cerr << "Invalid index. Packages key was either not found or is invalid" << std::endl;
        return false;
    }else if(j.find("urls") == j.end() || !j.at("urls").is_object())
    {
        std::cerr << "Invalid index. Urls key was either not found or is invalid" << std::endl;
        return false;
    }else if(j.find("versions") == j.end() || !j.at("versions").is_object())
    {
        std::cerr << "Invalid index. versions key was either not found or is invalid" << std::endl;
        return false;
    }

    try {
        packages = j.at("packages").get<std::vector<std::string>>();
        urls = j.at("urls").get<std::unordered_map<std::string, std::string>>();
        versions = j.at("versions").get<std::unordered_map<std::string, std::string>>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Error when parsing database JSON: " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool Index::operator!=(const Index &other)
{
    return (other.packages != packages || other.urls != urls || other.versions != versions);
}

bool Index::operator==(const Index &other)
{
    return (other.packages == packages && other.urls == urls && other.versions == versions);
}
