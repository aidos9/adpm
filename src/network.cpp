#include "../headers/network.h"
#include <iostream>
#include <curl/curl.h>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <iomanip>
#include <algorithm>

Network::Network()
{

}

bool Network::download(const std::string url, bool check_certs)
{
    std::string filename;
    if(!get_filename_from_url(url, filename))
    {
        std::cerr << "Something went wrong when getting the filename" << std::endl;
        std::cout << "Setting filename to \"download\"" << std::endl;
        filename = "download";
    }
    return download_filename(url,check_certs, filename);
}

bool Network::download_filename(const std::string url, bool check_certs, std::string filename)
{
    CURL *curl;
    FILE *fp;
    CURLcode res;
    curl = curl_easy_init();

    if(curl)
    {
        fp = fopen(filename.c_str(), "wb");
        if(fp == 0)
        {
            perror("adpm");
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, progress_func);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        res = curl_easy_setopt(curl, CURLOPT_NOPROGRESS, false);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        if(!check_certs)
        {
            res = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
            if(res != CURLE_OK)
            {
                std::cerr << curl_easy_strerror(res) << std::endl;
                return false;
            }
        }


        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        {
            std::cerr << curl_easy_strerror(res) << std::endl;
            return false;
        }

        curl_easy_cleanup(curl);
        fclose(fp);
        std::cout << std::endl;
        return true;
    }else{
        std::cerr << "Couldn't initialise curl" << std::endl;
        return false;
    }
}

std::string Network::get_filename_from_url(const std::string url)
{
    std::string filename = "";
    for(int i = url.size()-1; i >= 0; i--)
    {
        if(url[i] == '/')
        {
            std::reverse(filename.begin(), filename.end());
            return filename;
        }else{
            filename += url[i];
        }
    }
    return filename;
}

bool Network::get_filename_from_url(const std::string url, std::string &filename)
{
    //We will get the filename from the end
    //https://www.github.com/blah/blah.tar.bz2
    //Filename will be blah.tar.bz2 because we will work backwards

    for(int i = url.size()-1; i >= 0; i--)
    {
        if(url[i] == '/')
        {
            std::reverse(filename.begin(), filename.end());
            return true;
        }else{
            filename += url[i];
        }
    }
    return false;
}

size_t Network::write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

int Network::progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded)
{
    // ensure that the file to be downloaded is not empty
    // because that would cause a division by zero error later on
    if(NowDownloaded == 0)
    {
        return 0;
    }
    std::string suffix = "";
    std::string disp = "";
    int precision = 4;

    if((NowDownloaded / 1000) < 1000)
    {
        suffix = "KB";
        std::ostringstream out;
        out << std::setprecision(precision) << (NowDownloaded / 1000);
        disp = out.str();;
    }else if((NowDownloaded / 1000000) < 1000)
    {
        suffix = "MB";
        std::ostringstream out;
        out << std::setprecision(precision) << (NowDownloaded / 1000000);
        disp = out.str();
    }else if((NowDownloaded / 1000000000) < 1000)
    {
        suffix = "GB";
        std::ostringstream out;
        out << std::setprecision(precision) << (NowDownloaded / 1000000000);
        disp = out.str();
    }
    std::cout << "Total downloaded: " << disp << suffix;

    if (TotalToDownload <= 0.0) {
        printf("\r");
        fflush(stdout);
        return 0;
    }

    // how wide you want the progress meter to be
    double fractiondownloaded = NowDownloaded / TotalToDownload;
    // part of the progressmeter that's already "full"
    int dotz = round(fractiondownloaded * PROGRESSBAR_SIZE);

    // create the "meter"
    int ii=0;
    printf(" %3.0f%% [",fractiondownloaded*100);
    // part  that's full already
    for ( ; ii < dotz;ii++) {
        printf("=");
    }
    // remaining part (spaces)
    for ( ; ii < PROGRESSBAR_SIZE;ii++) {
        printf(" ");
    }
    // and back to line begin - do not forget the fflush to avoid output buffering problems!
    printf("]\r");
    fflush(stdout);
    // if you don't return 0, the transfer will be aborted - see the documentation
    return 0;
}
