#include "../headers/packageinfo.h"
#include <iostream>

PackageInfo::PackageInfo()
{

}

bool PackageInfo::is_valid()
{
    bool valid = true;

    if(files.size() == 0)
    {
        std::cerr << "No files were found in the compressed directory" << std::endl;
        valid = false;
    }

    if(name.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No name was provided" << std::endl;
        valid = false;
    }

    if(version.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No version was provided" << std::endl;
        valid = false;
    }

    if(description.find_first_not_of(' ') == std::string::npos)
    {
        std::cerr << "No description was provided" << std::endl;
        valid = false;
    }

    return valid;
}

nlohmann::json PackageInfo::to_json() const
{
    nlohmann::json j = {{"files", files}, {"description", description}, {"name", name}, {"version", version}};
    return j;
}

bool PackageInfo::from_json(const nlohmann::json &j)
{
    if(j.find("files") == j.end() || !j.at("files").is_array())
    {
        std::cerr << "Invalid package info file. Files key was either not found or is invalid" << std::endl;
        return false;
    }else if(j.find("description") == j.end() || !j.at("description").is_string())
    {
        std::cerr << "Invalid package info file. Description key was either not found or is invalid" << std::endl;
        return false;
    }else if(j.find("version") == j.end() || !j.at("version").is_string())
    {
        std::cerr << "Invalid package info file. Version key was either not found or is invalid" << std::endl;
        return false;
    }else if(j.find("name") == j.end() || !j.at("name").is_string())
    {
        std::cerr << "Invalid package info file. Name key was either not found or is invalid" << std::endl;
        return false;
    }

    try {
        files = j.at("files").get<std::vector<std::string>>();
        description = j.at("description").get<std::string>();
        version = j.at("version").get<std::string>();
        name = j.at("name").get<std::string>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Error when parsing database JSON: " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool PackageInfo::operator!=(const PackageInfo &other)
{
    return (other.files != files || other.version != version || other.name != name || other.description != description);
}

bool PackageInfo::operator==(const PackageInfo &other)
{
    return (other.files == files && other.version == version && other.name == name && other.description == description);
}
