#include "../headers/configfile.h"
#include <iostream>

Config::Config()
{

}

nlohmann::json Config::to_json() const
{
    nlohmann::json j = {{"with_color", with_color},{"database_path",db_path},{"variables",variables},
             {"remote_indexes",remote_indexes}};

    return j;
}

bool Config::from_json(const nlohmann::json &j)
{
    if(j.find("with_color") == j.end() || !j.at("with_color").is_boolean())
    {
        std::cerr << "Invalid package. Please set a boolean." << std::endl;
        return false;
    }else if(j.find("database_path") == j.end() || !j.at("database_path").is_string())
    {
        std::cerr << "Please set a string for database_path" << std::endl;
        return false;
    }

    if(j.find("remote_indexes") != j.end() && j.at("remote_indexes").is_array())
    {
        remote_indexes = j.at("remote_indexes").get<std::vector<std::string>>();
    }

    if(j.find("variables") != j.end() && j.at("variables").is_object())
    {
        variables = j.at("variables").get<std::unordered_map<std::string, std::string>>();
    }

    try {
        db_path = j.at("database_path").get<std::string>();
        with_color = j.at("with_color").get<bool>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "exception thrown when parsing config. Error: " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool Config::operator==(const Config &other)
{
    return(other.db_path == db_path && other.remote_indexes == remote_indexes && other.variables == variables && other.with_color == with_color);
}
bool Config::operator!=(const Config &other)
{
    return(other.db_path != db_path || other.remote_indexes != remote_indexes || other.variables != variables || other.with_color != with_color);
}
