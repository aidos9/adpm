#include "../headers/adpm.h"
#include "../headers/fileio.h"
#include "../config.h"
#include "../headers/algorithms.h"
#ifdef __linux__
#include <linux/limits.h>
#endif
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sys/stat.h>
#include <algorithm>
#include <unordered_map>
#include <sstream>

adpm::adpm()
{

}

int adpm::run(int argc, char* argv[])
{
    if(argc < 2)
    {
        std::cerr << format_string_colors("No operation provided", Color::red) << std::endl;
        return -1;
    }

    if(strcmp(argv[1], "make") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return make(argc, argv);
    }
#ifdef WITH_NETWORK
    else if(strcmp(argv[1], "download") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return download(argc, argv);
    }else if(strcmp(argv[1], "fetch") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return fetch(argc, argv);
    }else if(strcmp(argv[1], "search") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return search(argc, argv);
    }else if(strcmp(argv[1], "update") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return update(argc, argv);
    }
#endif
    else if(strcmp(argv[1], "install") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return install(argc, argv);
    }else if(strcmp(argv[1], "test") == 0)
    {
        return test(argc, argv);
    }else if(strcmp(argv[1], "clean") == 0)
    {
        return clean(argc, argv);
    }else if(strcmp(argv[1], "list") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return list(argc, argv);
    }else if(strcmp(argv[1], "remove") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return remove(argc, argv);
    }else if(strcmp(argv[1], "info") == 0)
    {
        if(!load_config())
        {
            return -1;
        }

        return info(argc, argv);
    }else if(strcmp(argv[1], "verify") == 0)
    {
        return verify(argc, argv);
    }else if(strcmp(argv[1], "--version") == 0 || strcmp(argv[1],"version") == 0 || strcmp(argv[1],"-v") == 0)
    {
        return version();
    }else if(strcmp(argv[1], "--help") == 0 || strcmp(argv[1],"help") == 0)
    {
        return help();
    }else{
        return help();
    }

}

int adpm::help()
{
    std::cout << format_string_colors("adpm [operation] [args]",Color::cyan) << std::endl;
    std::cout << format_string_colors("Operations", Color::red, true) << std::endl;
#if WITH_NETWORK
    std::cout << format_string_colors("download",operation_color, true) << format_string_colors("\t Download a package.adpm from the cached index files",description_color) << std::endl;
    std::cout << format_string_colors("fetch", operation_color, true) << format_string_colors("\t\t Downloads the index.adpm files", description_color) << std::endl;
    std::cout << format_string_colors("search", operation_color,true) << format_string_colors("\t\t Search for a file in the cached package lists", description_color) << std::endl;
    std::cout << format_string_colors("update", operation_color,true) << format_string_colors("\t\t Checks for new index files and version of package specified and fetches the package file.", description_color) << std::endl;
#endif
    std::cout << format_string_colors("make", operation_color, true) << format_string_colors("\t\t takes a package.adpm and executes it", description_color) << std::endl;
    std::cout << format_string_colors("test", operation_color, true) << format_string_colors("\t\t executes the tests provided by the package file", description_color) << std::endl;
    std::cout << format_string_colors("install", operation_color, true) << format_string_colors("\t takes the install.adpm directory and installs the files", description_color) << std::endl;
    std::cout << format_string_colors("clean", operation_color, true) << format_string_colors("\t\t cleans up files from a package", description_color) << std::endl;
    std::cout << format_string_colors("remove", operation_color, true) << format_string_colors("\t\t takes the name of a package and removes all files", description_color) << std::endl;
    std::cout << format_string_colors("list", operation_color, true) << format_string_colors("\t\t lists all the installed packages", description_color) << std::endl;
    std::cout << format_string_colors("info", operation_color, true) << format_string_colors("\t\t takes the name of a package and gives information about the package", description_color) << std::endl;
    std::cout << format_string_colors("verify", operation_color, true) << format_string_colors("\t\t verify that a package file is valid", description_color) << std::endl;
    std::cout << "For additonal help pass --help to each sub-command" << std::endl;
    return 0;
}

int adpm::version()
{
    std::cout << "adpm v" << VERSION << std::endl;
    return 0;
}

void adpm::replace_variables(std::string &line, const Package &pack)
{
    int dollarSignIndex = -1;
    bool findingVariable = false;
    std::string variable = "";
    for(int i = 0; i < line.size(); i++)
    {
        if(line[i] == '$' && dollarSignIndex == -1)
        {
            dollarSignIndex = i;
        }else if(dollarSignIndex >= 0 && !findingVariable && line[i] == '{')
        {
            findingVariable = true;
        }else if(dollarSignIndex >= 0 && findingVariable && line[i] == '}')
        {
            bool is_shell_variable = false;
            if(variable == "pkgdir")
            {
                variable = pkgdir();
            }else if(variable == "rootdir")
            {
                variable = root_path;
            }else if(variables.find(variable) != variables.end())
            {
                variable = variables.at(variable);
            }else if(variable == "pkgver" || variable == "package_version")
            {
                variable = pack.version;
            }else if(pack.with_variables_arg && pack.variables.find(variable) != pack.variables.end())
            {
                variable = pack.variables.at(variable);
            }else if(config_file.variables.find(variable) != config_file.variables.end())
            {
                variable = config_file.variables.at(variable);
            }else{
                is_shell_variable = true;
            }

            if(!is_shell_variable)
            {
                std::string tok = line;

                tok.erase(dollarSignIndex,(i-dollarSignIndex+1));
                tok.insert(dollarSignIndex,variable);

                line = tok;

            }
            findingVariable = false;
            dollarSignIndex = -1;

        }else if(findingVariable)
        {
            variable += line[i];
        }
    }

}

void adpm::make_global(std::vector<std::string> &paths)
{
    std::string cwd;
    FileIO::cwd(cwd);
    for(int i = 0; i < paths.size(); i++)
    {
        paths[i] = paths[i].substr(cwd.size());
    }
}

void adpm::make_paths(std::vector<std::string> &paths)
{
    for(int i = 0; i < paths.size(); i++)
    {
        std::string working_string = paths[i];
        if(working_string.size() == 0)
        {
            std::cerr << format_string_colors("error empty string received to make_paths", Color::red, true) << std::endl;
            exit(-1);
        }

        int pos = working_string.size()-1;
        while(working_string[pos] != '/')
        {
            working_string.pop_back();
            pos = working_string.size()-1;
        }

       std::string temp = "";
       temp += working_string[0];

       for(int pos = 1; pos < working_string.size(); pos++)
       {
            temp += working_string[pos];
            if(working_string[pos] == '/')
            {
                if(!FileIO::dir_exists(temp))
                {
                    int status = mkdir(temp.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                    if(status != 0 && errno != EEXIST)
                    {
                        std::cout << format_string_colors("Error when making directory", Color::red, true) << std::endl;
                        perror("adpm");
                        exit(-1);
                    }
                }
            }
       }
    }
}

void adpm::parse_vector_vars(const std::vector<std::string> vars)
{
    std::string key = "";
    bool found_equals = false;
    std::string val = "";

    for(int i = 0; i < vars.size(); i++)
    {
        for(int b = 0; b < vars[i].size(); b++)
        {
            if(!found_equals && vars[i][b] != '=')
            {
                key += vars[i][b];
            }else if(found_equals)
            {
                val += vars[i][b];
            }else if(vars[i][b] == '=')
            {
                found_equals = true;
            }
        }

        variables[key] = val;
        key = "";
        val = "";
        found_equals = false;
    }
}

std::string adpm::pkgdir()
{
    return (root_path + "/binaries.adpm");
}

bool adpm::load_config()
{
    if(!FileIO::file_exists(CONFIG_PATH))
    {
        std::cerr << "Could not find the config file" << std::endl;
        return false;
    }

    if(!FileIO::read_config(CONFIG_PATH, config_file))
    {
        return false;
    }

    return true;
}

std::string adpm::format_string_colors(std::string str, Color::Colors col, bool bold, bool foreground)
{
#ifdef WITH_COLOR
    if(!config_file.with_color)
    {
        return str;
    }else{
        std::stringstream ss;
        ss << Color::get_color(col, bold, foreground) << str << Color::def();
        return ss.str();
    }
#else
    return str;
    #endif
}
