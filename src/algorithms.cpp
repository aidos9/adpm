#include "../headers/algorithms.h"
#include <cmath>
#include <algorithm>
#include <iostream>

bool compare_string_function(std::string a, std::string b)
{
    return a < b;
}

std::string Algorithms::random_string(int length)
{
    static const std::string keyset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
    std::string response = "";

    while(response.size() < length)
    {
        response += keyset[rand() % (keyset.size()-1)];
    }

    return response;
}

int Algorithms::binary_search(const std::vector<std::string> &values, const std::string target)
{
    int first = 0;
    int last = values.size() - 1;
    int middle;
    bool found = false;
    int index = -1;

    while(!found && first <= last)
    {
        middle = floor((first+last)/2);
        if(values[middle] == target)
        {
            index = middle;
            found = true;
        }else if(values[middle] > target)
        {
            last = middle - 1;
        }else if(values[middle] < target)
        {
            first = middle + 1;
        }
    }

    return index;
}

void Algorithms::alpha_order_vector(std::vector<std::string> &values)
{
    std::sort(values.begin(), values.end(), compare_string_function);
}

bool check_string(std::string a, std::string substr)
{
    int size  = 0;
    if(a.size() > substr.size())
    {
        size = substr.size();
    }else{
        size = a.size();
    }

    for(int i = 0; i < size; i++)
    {
        if(a[i] != substr[i])
        {
            return false;
        }
    }

    return true;
}

std::vector<std::string> Algorithms::values_containing_substr(const std::vector<std::string> values, const std::string &substr)
{
    if(values.size() == 0)
    {
        return std::vector<std::string>();
    }else if(substr.size() == 0)
    {
        return values;
    }

    bool reached_first_letter = false;
    std::vector<std::string> results = std::vector<std::string>();
    for(int i = 0; i < values.size(); i++)
    {
        if(values[i][0] == substr[0])
        {
            reached_first_letter = true;
        }else if(values[i][0] > substr[0])
        {
            break;
        }

        if(reached_first_letter)
        {
            if(check_string(values[i], substr))
            {
                results.push_back(values[i]);
            }
        }
    }

    return results;
}
