#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/database.h"
#include <iostream>

int adpm::remove(int argc, char *argv[])
{
    for(int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << format_string_colors("Takes a program name", Color::green) << std::endl;
            return 0;
        }
    }

    std::string package_name = argv[2];
    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return -1;
    }

    Database db;
    FileIO::read_database(config_file.db_path,db);

    if(!db.contains(package_name))
    {
        std::cerr << format_string_colors("Could not find ", Color::red, true) << format_string_colors(package_name, Color::red, true) << std::endl;
        return -1;
    }

    std::string response;
    std::cout << format_string_colors("Are you sure you want to remove ", Color::magenta) << format_string_colors(package_name, Color::magenta) << format_string_colors(" (y/N) ", Color::magenta);
    std::getline(std::cin, response);

    if(response != "y" && response != "Y")
    {
        std::cerr << format_string_colors("Aborting", Color::red, true) << std::endl;
        return -1;
    }

    while( db.package_files[package_name].size() > 0)
    {
        if(!FileIO::delete_file(db.package_files[package_name][0]))
        {
            return -1;
        }else{
            db.package_files[package_name].erase(db.package_files[package_name].begin());
        }
    }

    if(!db.remove_package(package_name))
    {
        std::cerr << format_string_colors("Could not remove package from database", Color::red, true) << std::endl;
        return -1;
    }

    if(!FileIO::write_database(config_file.db_path,db))
    {
        std::cerr << format_string_colors("Database was not written", Color::red, true) << std::endl;
        return -1;
    }
    return 0;
}
