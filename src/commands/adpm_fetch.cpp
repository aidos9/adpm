#ifdef WITH_NETWORK
#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/network.h"
#include "../../headers/algorithms.h"
#include "../../config.h"
#include <iostream>

int adpm::fetch(int argc, char* argv[])
{
    bool disable_certs = false;

    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << format_string_colors("--disable-certs", Color::green, true) <<  format_string_colors("\t Disables checking SSL certificates", Color::cyan) << std::endl;
            std::cout << format_string_colors("-f, --fetch", Color::green, true) << format_string_colors("\t Fetch index files before searching", Color::cyan) << std::endl;
            return 0;
        }else if(strcmp(argv[i], "--disable-certs") == 0)
        {
            disable_certs = true;
        }
    }

    if(fetch_func(disable_certs))
    {
        return 0;
    }else{
        return -1;
    }
}

bool adpm::fetch_func(bool disable_certs)
{
    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return false;
    }

    Database db;
    if(!FileIO::read_database(config_file.db_path,db))
    {
        return false;
    }

    Network net = Network();

    for(int i = 0; i < config_file.remote_indexes.size(); i++)
    {
        std::string path = INDEX_FILES_DIRECTORY + Algorithms::random_string(12) + "-" + net.get_filename_from_url(config_file.remote_indexes[i]);

        int count = 0;
        while(FileIO::file_exists(path))
        {
            if(count == 1000)
            {
                std::cerr << format_string_colors("Couldn't create a unique filename", Color::red, true) << std::endl;
                return false;
            }
            path = INDEX_FILES_DIRECTORY + Algorithms::random_string(12) + "-" + net.get_filename_from_url(config_file.remote_indexes[i]);
            count++;
        }

        if(path == INDEX_FILES_DIRECTORY)
        {
            std::cerr << format_string_colors("Could not determine a file name for the index", Color::red, true) << std::endl;
            std::cerr << format_string_colors("Aborting", Color::red, true) << std::endl;
            return false;
        }

        std::cout << "Downloading: " << config_file.remote_indexes[i] << std::endl;
        if(!net.download_filename(config_file.remote_indexes[i], !disable_certs, path))
        {
            std::cerr << format_string_colors("Could not download: ", Color::red, true) << format_string_colors(config_file.remote_indexes[i], Color::red, true) << std::endl;
            return false;
        }

        if(FileIO::file_exists(db.index_files[config_file.remote_indexes[i]]))
        {
            if(!FileIO::delete_file(db.index_files[config_file.remote_indexes[i]]))
            {
                std::cerr << format_string_colors("Could not delete the old index file", Color::red, true) << std::endl;
                return false;
            }
        }

        db.index_files[config_file.remote_indexes[i]] = path;
    }

    if(!FileIO::write_database(config_file.db_path, db))
    {
        std::cerr << format_string_colors("Could not write database file", Color::red, true) << std::endl;
        return false;
    }

    return true;
}

#endif
