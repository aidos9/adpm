#include "../../headers/adpm.h"
#include "../../headers/database.h"
#include "../../headers/fileio.h"
#include <iostream>

int adpm::info(int argc, char *argv[])
{
    bool files = false;
    std::string package_name = "";
    for(int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << format_string_colors("Provides all the information about the package", Color::green) << std::endl;
            std::cout << format_string_colors("--files", operation_color, true) << format_string_colors("\t Show all the installed files",description_color) << std::endl;
            return 0;
        }else if(strcmp(argv[i],"--files") == 0)
        {
            files = true;
        }else{
            package_name = argv[i];
        }
    }

    if(package_name == "")
    {
        std::cerr << format_string_colors("No package name provided", Color::red, true) << std::endl;
        return -1;
    }

    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return -1;
    }

    Database db;

    if(!FileIO::read_database(config_file.db_path,db))
    {
        return -1;
    }

    if(!db.contains(package_name))
    {
        std::cerr << format_string_colors("This package is not installed", Color::red, true) << std::endl;
        return -1;
    }

    if(!files)
    {
        std::cout << format_string_colors("Package name: ", Color::green) << format_string_colors(package_name, Color::green) << std::endl;
        std::cout << format_string_colors("Version: ", Color::green) << format_string_colors(db.package_version[package_name], Color::green) << std::endl;
        std::cout << format_string_colors("Description: ", Color::green) << format_string_colors(db.package_info[package_name], Color::green) << std::endl;
    }else{
        std::cout << format_string_colors("Installed files", Color::green) << std::endl;

            for(int i = 0; i < db.package_files[package_name].size(); i++)
            {
                std::cout << format_string_colors(db.package_files[package_name][i], Color::green) << std::endl;
            }
    }

    return 0;
}
