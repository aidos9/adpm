#include "../../headers/adpm.h"
#include "../../headers/database.h"
#include "../../headers/fileio.h"
#include <iostream>

int adpm::list(int argc, char *argv[])
{
    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true)  << std::endl;
        return -1;
    }

    Database db;
    if(!FileIO::read_database(config_file.db_path,db))
    {
        return -1;
    }

    std::cout << format_string_colors("Package \t\tDescription", Color::green) << std::endl << std::endl;

    for(int i = 0; i < db.package_list.size(); i++)
    {
        std::cout << format_string_colors(db.package_list[i], Color::green) << format_string_colors("-", Color::green) << format_string_colors(db.package_version[db.package_list[i]], Color::green) << "\t\t" << format_string_colors(db.package_info[db.package_list[i]], Color::green)<< std::endl;
    }

    return 0;
}
