#include "../../headers/adpm.h"
#ifdef WITH_NETWORK
#include "../../headers/network.h"
#endif
#include "../../headers/fileio.h"
#include "../../headers/compression.h"
#include "../../headers/packageinfo.h"
#include <iostream>
#include <unistd.h>

int adpm::make(int argc, char* argv[])
{
    if(!FileIO::cwd(root_path))
    {
        std::cerr << format_string_colors("Something went wrong when setting the root path", Color::red, true) << std::endl;
        return -1;
    }

    bool ignore_deps = false;
#ifdef WITH_NETWORK
    bool ignore_network = false;
    bool disable_certs = false;
    bool with_fetch = true;
    bool remote = false;
    std::string name = "";
#endif
    std::string package_path = "package.adpm";
    std::vector<std::string> vec_vars = std::vector<std::string>();

    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i], "--help") == 0)
        {
            std::cout << "Options" << std::endl;
            std::cout << format_string_colors("--ignore-deps", operation_color, true) << format_string_colors("\t ignore the dependencies", description_color) << std::endl;
            std::cout << format_string_colors("-p, --package", operation_color, true) << format_string_colors("\t Pass in a custom package file", description_color)  << std::endl;
            std::cout << format_string_colors("-s",operation_color,true) << format_string_colors("\t\t Set a variable eg \"-s VERSION=5\"",description_color) << std::endl;
            #ifdef WITH_NETWORK
            std::cout << format_string_colors("-n, --ignore-network", operation_color, true) << format_string_colors("\t Skips if enabled downloading sources", description_color) << std::endl;
            std::cout << format_string_colors("--disable-certs",operation_color, true) << format_string_colors("\t Disables checking SSL certificates", description_color) << std::endl;
            std::cout << format_string_colors("-r, --remote",operation_color, true) << format_string_colors("\t Checks for a remote package file and downloads and compiles using that file", description_color) << std::endl;
            std::cout << format_string_colors("-nf, --no-fetch", operation_color, true) << format_string_colors("\t Disables fetching index files before making", description_color) << std::endl;
            #endif
            return 0;
        }else if(strcmp(argv[i],"--ignore-deps") == 0)
        {
            ignore_deps = true;
        }else if(strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--package") == 0)
        {
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("No filename provided", Color::red, true)  << std::endl;
                return -1;
            }else{
                package_path = argv[i+1];
                i++;
            }
        }else if(strcmp(argv[i], "-s") == 0)
        {
            if(i+1 >= argc)
            {
                std::cerr << format_string_colors("No variable provided", Color::red, true)  << std::endl;
                return -1;
            }

            vec_vars.push_back(argv[i+1]);
            i++;
        }
        #ifdef WITH_NETWORK
        else if(strcmp(argv[i], "-n") == 0 || strcmp(argv[i], "--ignore-network") == 0)
        {
            ignore_network = true;
        }else if(strcmp(argv[i], "--disable-certs") == 0)
        {
            disable_certs = true;
        }else if(strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--remote") == 0)
        {
            remote = true;
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("Error, no package name specified", Color::red, true) << std::endl;
                return -1;
            }else{
                name = argv[i+1];
                i++;
            }
        }else if(strcmp(argv[i], "-nf") == 0 || strcmp(argv[i], "--no-fetch") == 0)
        {
            with_fetch = false;
        }
        #endif
    }

    parse_vector_vars(vec_vars);
#ifdef WITH_NETWORK
    if(remote)
    {
        if(!download_func(disable_certs, with_fetch, name))
        {
            return -1;
        }else{
            package_path = dl_filename;
        }
    }
#endif

    FileIO f_obj;

    if(FileIO::dir_exists("binaries.adpm"))
    {
        std::cout << format_string_colors("The binaries.adpm directory already exists. Do you wish to delete it? (y/N) ",Color::green);
        std::string response;
        getline(std::cin,response);

        if(response != "y")
        {
            std::cerr << format_string_colors("Aborting", Color::red, true) << std::endl;
            return -1;
        }

        if(!f_obj.delete_directory("binaries.adpm"))
        {
            std::cerr << format_string_colors("Aborting", Color::red, true) << std::endl;
            return -1;
        }
    }

    if(FileIO::file_exists(package_path))
    {
        Package pack;
        if(!FileIO::read_package(package_path,pack))
        {
            return -1;
        }

        if(!pack.is_valid())
        {
            return -1;
        }

        if(!ignore_deps)
        {
            if(!FileIO::file_exists(config_file.db_path))
            {
                std::cerr << format_string_colors("The database file could not be found.", Color::red, true)  << std::endl;
                return -1;
            }else{
                Database db;
                FileIO::read_database(config_file.db_path, db);
                if(pack.with_build_dependencies_arg)
                {
                    for(int i = 0; i < pack.make_dependencies.size(); i++)
                    {
                        if(std::find(db.package_list.begin(),db.package_list.end(), pack.make_dependencies[i]) == db.package_list.end())
                        {
                            std::cerr << format_string_colors("Could not find: ", Color::red, true)  << format_string_colors(pack.make_dependencies[i], Color::red, true)  << std::endl;
                            return -1;
                        }
                    }
                }

                if(pack.with_dependencies_arg)
                {
                    for(int i = 0; i < pack.runtime_dependencies.size(); i++)
                    {
                        if(std::find(db.package_list.begin(),db.package_list.end(), pack.runtime_dependencies[i]) == db.package_list.end())
                        {
                            std::cerr << format_string_colors("Could not find: ", Color::red, true)  << format_string_colors(pack.runtime_dependencies[i], Color::red, true)  << std::endl;
                            return -1;
                        }
                    }
                }
            }
        }

        if(pack.with_variables_arg)
        {
            for(auto& it : pack.variables)
            {
                replace_variables(it.second,pack);
            }
        }

        #ifdef WITH_NETWORK
        if(pack.with_sources_arg && !ignore_network)
        {
            Network net = Network();
            for(int i = 0; i < pack.sources.size(); i++)
            {
                std::string command = pack.sources[i];
                replace_variables(command, pack);

                if(!net.download(command, !disable_certs))
                {
                    std::cerr << format_string_colors("aborting", Color::red, true)  << std::endl;
                    return -1;
                }
            }
        }
        #endif

        if(mkdir(pkgdir().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0 && errno != EEXIST)
        {
            perror("adpm");
            return -1;
        }

        for(int i = 0; i < pack.prepare_commands.size(); i++)
        {
            std::string command = pack.prepare_commands[i];
            replace_variables(command, pack);
            if(command.size() > 3 && command[0] == 'c' && command[1] == 'd')
            {
                command.erase(0,3);
                chdir(command.c_str());
            }else if(system(command.c_str()) != 0){
                std::cerr << format_string_colors("A prepare command returned a non-zero exit code. Aborting", Color::red, true)  << std::endl;
                return -1;
            }
        }

        for(int i = 0; i < pack.build_commands.size(); i++)
        {
            std::string command = pack.build_commands[i];
            replace_variables(command, pack);
            if(command.size() > 3 && command[0] == 'c' && command[1] == 'd')
            {
                command.erase(0,3);
                if(chdir(command.c_str()) != 0)
                {
                    std::cerr << format_string_colors("A build command returned a non-zero exit code. Aborting", Color::red, true)  << std::endl;
                    return -1;
                }
            }else if(system(command.c_str()) != 0){

                std::cerr << format_string_colors("A build command returned a non-zero exit code. Aborting", Color::red, true)  << std::endl;
                return -1;
            }
        }

        for(int i = 0; i < pack.package_commands.size(); i++)
        {
            std::string command = pack.package_commands[i];
            replace_variables(command, pack);
            
            if(command.size() > 3 && command[0] == 'c' && command[1] == 'd')
            {
                command.erase(0,3);
                if(chdir(command.c_str()) != 0)
                {
                    std::cerr << format_string_colors("A package command returned a non-zero exit code. Aborting", Color::red, true)  << std::endl;
                    return -1;
                }
            }else if(system(command.c_str()) != 0){
                std::cout << format_string_colors("A package command returned a non-zero exit code. Aborting", Color::red, true) << std::endl;
                return -1;
            }
        }

        if(chdir(root_path.c_str()) != 0)
        {
            perror("adpm");
            return -1;
        }

        PackageInfo pkginfo;
        pkginfo.name = pack.name;
        pkginfo.description = pack.description;
        pkginfo.version = pack.version;

        if(!FileIO::dir_exists("binaries.adpm"))
        {
            std::cerr << format_string_colors("Could not find the binaries.adpm directory", Color::red, true);
            return -1;
        }

        pkginfo.files = f_obj.path_list("binaries.adpm");

        if(!FileIO::write_pkginfo("binaries.adpm/pkginfo.adpm", pkginfo))
        {
            std::cerr << format_string_colors("Aborting", Color::red, true);
            return -1;
        }
    }else{
        std::cerr << format_string_colors("Could not find \"package.adpm\" package file. Please create one", Color::red, true) << std::endl;
        return -1;
    }

    std::cout << format_string_colors("Compressing", Color::magenta) << std::endl;

    FileIO obj = FileIO();

    std::vector<std::string> files = obj.path_list("binaries.adpm");

    Compression::create("binaries.comp", files);

    if(!obj.delete_directory("binaries.adpm"))
    {
        std::cerr << format_string_colors("Could not delete working directory", Color::red, true) << std::endl;
        return -1;
    }

    std::cout << format_string_colors("Success. Now run install", Color::green, true) << std::endl;

    return 0;
}
