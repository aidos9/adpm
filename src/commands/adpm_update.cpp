#ifdef WITH_NETWORK
#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/database.h"
#include "../../headers/network.h"
#include <iostream>

int adpm::update(int argc, char* argv[])
{
    bool disable_certs = false;
    bool fetch = true;
    std::string name = "";

    for(int i = 2; i < argc; i++)
    {
        if(strcmp("--help",argv[i]) == 0)
        {
            std::cout << format_string_colors("adpm update [package_name] [options]", Color::magenta) << std::endl;
            std::cout << format_string_colors("--disable-certs", Color::green, true) <<  format_string_colors("\t Disables checking SSL certificates", Color::cyan) << std::endl;
            std::cout << format_string_colors("-nf, --no-fetch", Color::green, true) << format_string_colors("\t Don't fetch index files before searching", Color::cyan) << std::endl;
            return 0;
        }else if(strcmp(argv[i], "--disable-certs") == 0)
        {
            disable_certs = true;
        }else if(strcmp(argv[i], "-nf") == 0 || strcmp(argv[i], "--no-fetch") == 0)
        {
            fetch = false;
        }else{
            name = argv[i];
        }
    }

    if(name == "")
    {
        std::cerr << format_string_colors("No package name supplied", Color::red, true) << std::endl;
        return -1;
    }

    if(fetch)
    {
        fetch_func(disable_certs);
    }

    std::cout << format_string_colors("Updating \"", Color::green) << format_string_colors(name, Color::green) << format_string_colors("\"",Color::green) << std::endl << std::endl;

    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return -1;
    }

    Database db;

    if(!FileIO::read_database(config_file.db_path, db))
    {
        return -1;
    }

    if(std::find(db.package_list.begin(), db.package_list.end(), name) == db.package_list.end())
    {
        std::cerr << format_string_colors("No package called \"", Color::red, true) << format_string_colors(name, Color::red, true) << format_string_colors("\" is installed", Color::red, true) << std::endl;
        return -1;
    }

    for(auto url : config_file.remote_indexes)
    {
        if(db.index_files.find(url) == db.index_files.end())
        {
            std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
            return -1;
        }else{
            Index ind;
            if(!FileIO::file_exists(db.index_files[url]))
            {
                std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
                return -1;
            }

            if(!FileIO::read_index(db.index_files[url], ind))
            {
                return -1;
            }

            if(std::find(ind.packages.begin(), ind.packages.end(), name) != ind.packages.end())
            {
                if(ind.versions[name] != db.package_version[name])
                {
                    Network net = Network();
                    if(!net.download(ind.urls[name], !disable_certs))
                    {
                        return -1;
                    }else{
                        std::cout << format_string_colors("Successfully downloaded. Now run adpm make.", Color::magenta, true) << std::endl;
                        return 0;
                    }
                }else{
                    std::cout << format_string_colors("Package is up to date", Color::magenta, true) << std::endl;
                    return 0;
                }
            }
        }
    }

    std::cerr << format_string_colors("Could not find any packages named \"", Color::red, true) << format_string_colors(name, Color::red, true) << format_string_colors("\"", Color::red, true) << std::endl;
    return -1;
}
#endif
