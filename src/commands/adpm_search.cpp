#ifdef WITH_NETWORK
#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/network.h"
#include "../../headers/algorithms.h"
#include <iostream>

int adpm::search(int argc, char* argv[])
{
    bool disable_certs = false;
    bool fetch = false;
    std::string name = "";

    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << format_string_colors("Usage adpm search [package_name] [options]", Color::green) << std::endl;
            std::cout << format_string_colors("Searches the cached index files ", Color::green) << std::endl;
            std::cout << format_string_colors("--disable-certs", Color::green, true) <<  format_string_colors("\t Disables checking SSL certificates", Color::cyan) << std::endl;
            std::cout << format_string_colors("-f, --fetch", Color::green, true) << format_string_colors("\t Fetch index files before searching", Color::cyan) << std::endl;

            return 0;
        }else if(strcmp(argv[i], "--disable-certs") == 0)
        {
            disable_certs = true;
        }else if(strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--fetch") == 0)
        {
            fetch = true;
        }else{
            name = argv[i];
        }
    }

    if(fetch)
    {
        if(!fetch_func(disable_certs))
        {
            return -1;
        }
    }

    std::cout << format_string_colors("Searching for \"", Color::green) << format_string_colors(name, Color::green) << format_string_colors("\"",Color::green) << std::endl << std::endl;

    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return -1;
    }

    Database db;

    if(!FileIO::read_database(config_file.db_path, db))
    {
        return -1;
    }

    bool found = false;

    for(auto url : config_file.remote_indexes)
    {
        if(db.index_files.find(url) == db.index_files.end())
        {
            std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
            return -1;
        }else{
            Index ind;
            if(!FileIO::file_exists(db.index_files[url]))
            {
                std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
                return -1;
            }

            if(!FileIO::read_index(db.index_files[url], ind))
            {
                return -1;
            }
            std::vector<std::string> organised = ind.packages;
            Algorithms::alpha_order_vector(organised);

            std::vector<std::string> results = Algorithms::values_containing_substr(organised,name);
            for(auto result : results)
            {
                std::cout << format_string_colors(result, Color::green) << format_string_colors(" - v", Color::green) << format_string_colors(ind.versions[result], Color::green) << format_string_colors(" - ", Color::green) << format_string_colors(ind.urls[result],Color::green) << std::endl;
            }

            if(results.size() > 0)
            {
                found = true;
            }
        }
    }

    if(!found)
    {
        std::cerr << format_string_colors("Could not find any packages", Color::red, true) << std::endl;
        return -1;
    }

    return 0;
}

#endif
