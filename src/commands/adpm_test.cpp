#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/package.h"
#include "../../headers/compression.h"
#include <iostream>

int adpm::test(int argc, char* argv[])
{
    std::string package_path = "package.adpm";
    std::string compressed_file = "binaries.comp";

    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i], "--help") == 0)
        {
            std::cout << "Options" << std::endl;
            std::cout << format_string_colors("-p, --package", operation_color, true) << format_string_colors("\t Pass in a custom package file", description_color)  << std::endl;
            std::cout << format_string_colors("-s",operation_color,true) << format_string_colors("\t\t Set a variable eg \"-s VERSION=5\"",description_color) << std::endl;
            std::cout << format_string_colors("-c",operation_color,true) << format_string_colors("\t\t Pass in a different compressed directory",description_color) << std::endl;
            return 0;
        }else if(strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--package") == 0)
        {
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("No filename provided", Color::red, true)  << std::endl;
                return -1;
            }else{
                package_path = argv[i+1];
                i++;
            }
        }else if(strcmp(argv[i], "-c") == 0)
        {
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("No filename provided", Color::red, true) << std::endl;
                return -1;
            }else{
                compressed_file = argv[i+1];
                i++;
            }
        }
    }

    Package pack;

    if(!FileIO::read_package(package_path, pack))
    {
        return -1;
    }

    if(!pack.is_valid())
    {
        return -1;
    }

    if(!FileIO::file_exists(compressed_file))
    {
        std::cerr << format_string_colors("Could not find the compressed binaries file. Please run make or specify the file", Color::red, true) << std::endl;
        return -1;
    }

    for(int i = 0; i < pack.test_commands.size(); i++)
    {
        std::string command = pack.test_commands[i];
        replace_variables(command, pack);
        if(command.size() > 3 && command[0] == 'c' && command[1] == 'd')
        {
            command.erase(0,3);
            chdir(command.c_str());
        }else if(system(command.c_str()) != 0){
            std::cerr << format_string_colors("The tests failed. Aborting", Color::red, true)  << std::endl;
            return -1;
        }
    }

    std::cout << format_string_colors("All tests passed", Color::green) << std::endl;
    return 0;
}
