#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include <iostream>

int adpm::verify(int argc, char* argv[])
{
    std::string file = "";
    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << format_string_colors("pass a .apdm file for verification", Color::green) << std::endl;
            return 0;
        }else{
            file = argv[i];
        }
    }

    if(!FileIO::file_exists(file))
    {
        std::cerr << format_string_colors("File doesn't exist", Color::red, true) << std::endl;
    }

    nlohmann::json j;
    if(!FileIO::read_json(file, j))
    {
        return -1;
    }

    bool any_errors = false;

    if(j.find("sources") == j.end())
    {
        std::cerr << format_string_colors("Sources was not provided", Color::red, true) << std::endl;
    }else{
        if(!j.at("sources").is_array())
        {
            std::cerr << format_string_colors("Sources is not an array", Color::red, true) << std::endl;
        }
    }

    if(j.find("variables") != j.end())
    {
        std::cout << format_string_colors("Variables detected", Color::green) << std::endl;
        if(!j.at("variables").is_object())
        {
            std::cerr << format_string_colors("Variables is not an object", Color::red, true) << std::endl;
        }
    }else{
        std::cerr << format_string_colors("Variables not found", Color::red, true) << std::endl;
    }

    if(j.find("package_name") == j.end() || !j.at("package_name").is_string())
    {
        std::cerr << format_string_colors("Invalid package_name. Please set a string.", Color::red, true)<< std::endl;
        any_errors = true;
    }

    if(j.find("package_version") == j.end() || !j.at("package_version").is_string())
    {
        std::cerr << format_string_colors("Invalid package_version. Please set a string.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("package_desc") == j.end() || !j.at("package_desc").is_string())
    {
        std::cerr << format_string_colors("Invalid package_desc. Please set a string.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("dependencies") == j.end() || !j.at("dependencies").is_array())
    {
        std::cerr << format_string_colors("Invalid dependencies. Please set an array of strings or provide an empty array.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("build_dependencies") == j.end() || !j.at("build_dependencies").is_array())
    {
        std::cerr << format_string_colors("Invalid build_dependencies. Please set an array of strings or provide an empty array.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("prepare") == j.end() || !j.at("prepare").is_array())
    {
        std::cerr << format_string_colors("Invalid prepare. Please set an array of strings or provide an empty array.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("build") == j.end() || !j.at("build").is_array())
    {
        std::cout << format_string_colors("Invalid build. Please set an array of strings or provide an empty array.", Color::red, true) << std::endl;
        any_errors = true;
    }

    if(j.find("package") == j.end() || !j.at("package").is_array())
    {
        std::cerr << format_string_colors("Invalid package. Please set an array of strings or provide an empty array.", Color::red, true) << std::endl;
        any_errors = true;
    }

    return 0;
}
