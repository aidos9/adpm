#include "../../headers/adpm.h"
#include "../../headers/package.h"
#include "../../headers/fileio.h"
#ifdef WITH_NETWORK
#include "../../headers/network.h"
#endif
#include <iostream>
#include <string>

int adpm::clean(int argc, char* argv[])
{
    std::string package_path = "package.adpm";
    std::vector<std::string> vec_vars = std::vector<std::string>();

    for(int i = 2; i < argc; i++)
    {
        if(strcmp(argv[i], "--help") == 0)
        {
            std::cout << "Options" << std::endl;
            std::cout << format_string_colors("-p, --package", operation_color, true) << format_string_colors("\t Pass in a custom package file", description_color)  << std::endl;
            std::cout << format_string_colors("-s",operation_color,true) << format_string_colors("\t\t Set a variable eg \"-s VERSION=5\"",description_color) << std::endl;
            return 0;
        }else if(strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--package") == 0)
        {
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("No filename provided", Color::red, true)  << std::endl;
                return -1;
            }else{
                package_path = argv[i+1];
                i++;
            }
        }else if(strcmp(argv[i], "-s") == 0)
        {
            if(i+1 >= argc)
            {
                std::cerr << format_string_colors("No variable provided", Color::red, true)  << std::endl;
                return -1;
            }

            vec_vars.push_back(argv[i+1]);
            i++;
        }
    }

    parse_vector_vars(vec_vars);

    Package pack;
    if(!FileIO::file_exists(package_path))
    {
        std::cerr << format_string_colors("Could not find the package file", Color::red, true) << std::endl;
        return -1;
    }

    if(!FileIO::read_package(package_path, pack))
    {
        std::cerr << format_string_colors("Could not read the package file", Color::red, true) << std::endl;
        return -1;
    }

    std::vector<std::string> files = {"binaries.adpm", "binaries.comp"};

#ifdef WITH_NETWORK
    for(std::string source : pack.sources)
    {
        replace_variables(source, pack);
        files.push_back(Network::get_filename_from_url(source));
    }
#endif

    if(pack.with_clean)
    {
        for(std::string file : pack.cleanup_files)
        {
            replace_variables(file, pack);
            files.push_back(file);
        }
    }

    FileIO file_obj = FileIO();
    for(std::string file : files)
    {
        std::cout << format_string_colors("Deleting: ", Color::green , true) << format_string_colors(file, Color::magenta , true) << std::endl;
        if(!file_obj.delete_misc(file))
        {
            std::cerr << format_string_colors("Something went wrong when deleting. Aborting", Color::red, true) << std::endl;
            return -1;
        }
    }

    return 0;
}
