#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/compression.h"
#include <iostream>

int adpm::install(int argc, char *argv[])
{
    if(!FileIO::cwd(root_path))
    {
        std::cerr << format_string_colors("Something went wrong when setting the root path", Color::red, true) << std::endl;
        return -1;
    }

    bool force = false;
    std::string compressed_file = "binaries.comp";
    std::vector<std::string> vec_vars = std::vector<std::string>();

    for(int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i],"--help") == 0)
        {
            std::cout << "Options" << std::endl;
            std::cout << format_string_colors("--ignore-deps", operation_color, true) << format_string_colors("\t ignore the dependencies", description_color) << std::endl;
            std::cout << format_string_colors("-s",operation_color,true) << format_string_colors("\t\t Set a variable eg \"-s VERSION=5\"",description_color) << std::endl;
            std::cout << format_string_colors("-c",operation_color,true) << format_string_colors("\t\t Pass in a different compressed directory",description_color) << std::endl;
            return 0;
        }else if(strcmp(argv[i],"-f") == 0)
        {
            force = true;
        }else if(strcmp(argv[i], "-s") == 0)
        {
            if(i+1 >= argc)
            {
                std::cerr << format_string_colors("No variable provided", Color::red, true)  << std::endl;
                return -1;
            }

            vec_vars.push_back(argv[i+1]);
            i++;
        }else if(strcmp(argv[i], "-c") == 0)
        {
            if(argc <= (i+1))
            {
                std::cerr << format_string_colors("No filename provided", Color::red, true) << std::endl;
                return -1;
            }else{
                compressed_file = argv[i+1];
                i++;
            }
        }
    }

    parse_vector_vars(vec_vars);

    if(!FileIO::file_exists(compressed_file))
    {
        std::cerr << format_string_colors("Could not find the compressed directory", Color::red, true)  << std::endl;
        return -1;
    }

    std::cout << format_string_colors("Decompressing", Color::magenta) << std::endl;

    Compression::extract(compressed_file);

    if(!FileIO::dir_exists("binaries.adpm"))
    {
        std::cerr << format_string_colors("Could not find the extracted directory", Color::red, true)  << std::endl;
        return -1;
    }

    if(chdir("binaries.adpm") != 0)
    {
        std::cerr << format_string_colors("Could not change directory", Color::red, true)  << std::endl;
        perror("adpm");
        return -1;
    }

    if(!FileIO::file_exists("pkginfo.adpm"))
    {
        std::cerr << format_string_colors("Could not find the package info file", Color::red, true)  << std::endl;
        return -1;
    }

    PackageInfo pkginfo;
    if(!FileIO::read_pkginfo("pkginfo.adpm", pkginfo))
    {
        std::cerr << format_string_colors("Aborting", Color::red, true) << std::endl;
        return -1;
    }

    if(!pkginfo.is_valid())
    {
        return -1;
    }

    FileIO file = FileIO();
    std::vector<std::string> local_files;
    std::string working;

    for(auto f_str : pkginfo.files)
    {
        working = root_path + "/" + f_str;
        local_files.push_back(working);
    }

    std::vector<std::string> files = local_files;
    make_global(files);

    Database db;

    if(!FileIO::file_exists(config_file.db_path))
    {
        db = Database();
    }else{
        if(!FileIO::read_database(config_file.db_path,db))
        {
            return -1;
        }
    }

    db.package_files[pkginfo.name] = files;
    db.package_info[pkginfo.name] = pkginfo.description;
    db.package_version[pkginfo.name] = pkginfo.version;
    bool exists = db.contains(pkginfo.name);

    if(!exists)
    {
        db.package_list.push_back(pkginfo.name);
    }else if(!force){

        if(!file.delete_directory("binaries.adpm"))
        {
            std::cerr << format_string_colors("Could not delete the extracted directory", Color::red, true) << std::endl;
        }

        std::cerr << format_string_colors("This package is already installed. To force installation supply -f", Color::red, true)  << std::endl;
        return -1;
    }

    if(!force || !exists)
    {
        std::cout << format_string_colors("Are you sure you want to install ", Color::magenta) << format_string_colors(pkginfo.name, Color::magenta) << format_string_colors(" (y/N) ", Color::magenta);
    }else if(force && exists){
        std::cout  << format_string_colors("Are you sure you want to overwrite ", Color::magenta) << format_string_colors(pkginfo.name, Color::magenta) << format_string_colors(" (y/N) ", Color::magenta);
    }

    std::string response;
    std::getline(std::cin,response);
    if(response != "y" && response != "Y")
    {
        std::cerr << format_string_colors("Aborting", Color::red, true)  << std::endl;
        return -1;
    }

    make_paths(files);

    for(int i = 0; i < local_files.size(); i++)
    {
        std::string progress_part = "[" + std::to_string(i) + "/" + std::to_string(local_files.size()) + "] ";
        fflush(stdout);
        std::cout << format_string_colors(progress_part, Color::green, true) << format_string_colors(files[i], Color::green) << "\r";

        if(!FileIO::copy_file(local_files[i],files[i]))
        {
            return -1;
        }
    }

    std::cout << std::endl;

    if(!FileIO::write_database(config_file.db_path, db))
    {
        return -1;
    }

    if(chdir(root_path.c_str()) != 0)
    {
        perror("adpm");
        return -1;
    }


    FileIO obj = FileIO();

    if(!obj.delete_directory("binaries.adpm"))
    {
        std::cerr << format_string_colors("Could not delete working directory", Color::red, true) << std::endl;
        return -1;
    }

    std::cout << format_string_colors("Successfully installed the package", Color::green, true) << std::endl;

    return 0;
}
