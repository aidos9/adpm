#ifdef WITH_NETWORK
#include "../../headers/adpm.h"
#include "../../headers/fileio.h"
#include "../../headers/database.h"
#include "../../headers/network.h"
#include <iostream>

int adpm::download(int argc, char* argv[])
{
    bool disable_certs = false;
    bool fetch = false;
    std::string name = "";

    for(int i = 2; i < argc; i++)
    {
        if(strcmp("--help",argv[i]) == 0)
        {
            std::cout << format_string_colors("--disable-certs", Color::green, true) <<  format_string_colors("\t Disables checking SSL certificates", Color::cyan) << std::endl;
            std::cout << format_string_colors("-f, --fetch", Color::green, true) << format_string_colors("\t Fetch index files before searching", Color::cyan) << std::endl;
            return 0;
        }else if(strcmp(argv[i], "--disable-certs") == 0)
        {
            disable_certs = true;
        }else if(strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--fetch") == 0)
        {
            fetch = true;
        }else{
            name = argv[i];
        }
    }

   if(download_func(disable_certs, fetch, name))
   {
       std::cout << format_string_colors("Successfully downloaded ", Color::green) << format_string_colors(dl_filename, Color::green, true) << std::endl << format_string_colors("Now run adpm make -p ", Color::green) << format_string_colors(dl_filename, Color::green) << std::endl;
   }else{
        return -1;
   }

   return 0;
}

bool adpm::download_func(bool disable_certs, bool fetch_val, const std::string &name)
{
    if(fetch_val)
    {
        fetch_func(disable_certs);
    }

    std::cout << format_string_colors("Downloading \"", Color::green) << format_string_colors(name, Color::green) << format_string_colors("\"", Color::green) << std::endl;

    if(!FileIO::file_exists(config_file.db_path))
    {
        std::cerr << format_string_colors("Could not find the database file", Color::red, true) << std::endl;
        return false;
    }

    Database db;

    if(!FileIO::read_database(config_file.db_path, db))
    {
        return false;
    }

    bool found = false;
    Network net = Network();

    for(auto url : config_file.remote_indexes)
    {
        if(db.index_files.find(url) == db.index_files.end())
        {
            std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
            return false;
        }else{
            Index ind;
            if(!FileIO::file_exists(db.index_files[url]))
            {
                std::cerr << format_string_colors("Could not find the index file for a remote source. Please run fetch", Color::red, true) << std::endl;
                return false;
            }

            if(!FileIO::read_index(db.index_files[url], ind))
            {
                return false;
            }

            if(int location = std::find(ind.packages.begin(), ind.packages.end(), name) != ind.packages.end())
            {
                std::string filename;
                net.get_filename_from_url(ind.urls[name], filename);

                if(!net.download(ind.urls[name],!disable_certs))
                {
                    std::cerr << format_string_colors("Failed to download.", Color::red, true) << std::endl;
                    return false;
                }else{
                    dl_filename = filename;
                }
                found = true;
            }
        }
    }

    if(!found)
    {
        std::cerr << format_string_colors("Could not find the required package", Color::red, true) << std::endl;
        return false;
    }

    return true;
}

#endif
