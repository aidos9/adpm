#include "../headers/color.h"
#ifdef WITH_COLOR
#include <termcap.h>
#endif
#include <iostream>
#include <unordered_map>

#ifdef WITH_COLOR
std::string Color::get_color(Colors col, bool bold, bool foreground)
{
    if(!supports_colors())
    {
        return "";
    }

    std::string ret = "\033";
    if(bold)
    {
        ret += "[1;";
    }else{
        ret += "[0;";
    }

    switch (col) {
        case Colors::black:
            if(foreground)
            {
                ret += "30m";
            }else{
                ret += "40m";
            }
            return ret;
        case Colors::red:
            if(foreground)
            {
                ret += "31m";
            }else{
                ret += "41m";
            }
            return ret;
        case Colors::green:
            if(foreground)
            {
                ret += "32m";
            }else{
                ret += "42m";
            }
            return ret;
        case Colors::yellow:
            if(foreground)
            {
                ret += "33m";
            }else{
                ret += "43m";
            }
            return ret;
        case Colors::blue:
            if(foreground)
            {
                ret += "34m";
            }else{
                ret += "44m";
            }
            return ret;
        case Colors::magenta:
            if(foreground)
            {
                ret += "35m";
            }else{
                ret += "45m";
            }
            return ret;
        case Colors::cyan:
            if(foreground)
            {
                ret += "36m";
            }else{
                ret += "46m";
            }
            return ret;
        case Colors::white:
            if(foreground)
            {
                ret += "37m";
            }else{
                ret += "47m";
            }
            return ret;
    }

    return "";
}

std::string Color::def()
{
    return "\033[0m";
}

bool Color::supports_colors()
{
    char* termtype = getenv("TERM");
    if(termtype == nullptr)
    {
        return false;
    }

    char buffer[2048];
    if (tgetent(buffer,termtype) < 1)
    {
        return false;
    }

    char code[] = "Co";
    return (tgetnum(code) > 0);
}
#endif
