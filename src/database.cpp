#include "../headers/database.h"
#include <iostream>
#include <algorithm>

Database::Database()
{

}

bool Database::contains(const std::string package)
{
    return (std::find(package_list.begin(), package_list.end(), package) != package_list.end());
}

bool Database::remove_package(const std::string package)
{
    if(contains(package))
    {
        package_list.erase(std::find(package_list.begin(), package_list.end(), package));
        package_version.erase(package);
        package_info.erase(package);
        package_files.erase(package);
        return true;
    }else{
        return false;
    }
}

bool Database::from_json(const nlohmann::json &j)
{
    if(j.find("package_list") == j.end() || !j.at("package_list").is_array())
    {
        std::cout << "Invalid package_list. The database has been corrupted." << std::endl;
        return false;
    }else if(j.find("package_files") == j.end() || !j.at("package_files").is_object())
    {
        std::cout << "Invalid package_files. The database has been corrupted." << std::endl;
        return false;
    }else if(j.find("package_info") == j.end() || !j.at("package_info").is_object())
    {
        std::cout << "Invalid package_info. The database has been corrupted." << std::endl;
        return false;
    }else if(j.find("package_version") == j.end() || !j.at("package_version").is_object())
    {
        std::cout << "Invalid package_version. The database has been corrupted." << std::endl;
        return false;
    }

    if(j.find("index_files") != j.end() && j.at("index_files").is_object())
    {
        try {
            index_files = j.at("index_files").get<std::unordered_map<std::string, std::string>>();
        } catch (nlohmann::json::exception &e) {
            std::cerr << "Error when parsing database JSON: " << e.what() << std::endl;
            return false;
        }
    }

    try {
        package_list = j.at("package_list").get<std::vector<std::string>>();
        package_files = j.at("package_files").get<std::unordered_map<std::string,std::vector<std::string>>>();
        package_info = j.at("package_info").get<std::unordered_map<std::string, std::string>>();
        package_version = j.at("package_version").get<std::unordered_map<std::string, std::string>>();
    } catch (nlohmann::json::exception &e) {
        std::cerr << "Error when parsing database JSON: " << e.what() << std::endl;
        return false;
    }
    return true;
}

nlohmann::json Database::to_json() const
{
    nlohmann::json j = {{"package_list",package_list},{"package_files",package_files},{"package_info",package_info},
             {"package_version",package_version}};

    if(index_files.size() != 0)
    {
        j["index_files"] = index_files;
    }

    return j;
}

bool Database::operator!=(const Database &other)
{
    return (other.package_list != package_list || other.index_files != index_files || other.package_files != package_files
            || other.package_info != package_info || other.package_version != package_version);
}

bool Database::operator==(const Database &other)
{
    return (other.package_list == package_list && other.index_files == index_files && other.package_files == package_files
            && other.package_info == package_info && other.package_version == package_version);
}
