# adpm
A source package manager.

## NOTICE
If you don't write your own package.adpm files and you download them from elsewhere, malicious code could be wrong. Check all package.adpm files before use.

## Commands
```
make - Run the package.adpm file.
install - Install the package.
remove - Uninstall a package.
list - List all the packages.
info - Display information about a selected package.
fetch - Fetch the index files from the links in the config file
search - Searches the index files for a specified package.
download - Download a package.adpm file from an index file.
update - Checks for new index files and version of package specified and fetches the package file.
clean - Cleans the files created by adpm and the downloaded source files
test - Runs the tests provided by the package file
```

## Example Usage
To install a package
```
adpm fetch #Download the repo index files from the repos specified
adpm make -r [package name] #Fetches the package file and runs make
adpm install # Installs the package in the binaries.comp file
```

To view the installed packages
```
adpm list
```

To see if a repo has a desired package
```
adpm search "" # Lists all the packages in all the repos
adpm search "[package_name]" Searches for package names containing the provided search
```

To remove a package
```
adpm remove [package_name]
```

## Config file
The default path is /etc/config.adpm. It is written in the form of a JSON file and must include if colour is enabled, the database path and either an empty array of remote index links or an array of links. Optionally it can include some global variables that you want defined. 

## Writing a package.adpm file
See the examples provided in the "examples" subdirectory. The template displays all the required information. 
Each argument in the "build", "prepare" and "package" arrays is executed with the system commmand. For a working example see the <a href="https://github.com/aidos9/adpm-repo">adpm-repo</a>.

### Example
```
{
	"package_name": "Package name",
	"package_version": "v1.0",
	"package_desc": "My pacakge description",
	"dependencies": ["depend"],
	"sources" : [
		"http://package.com/pkg-${pkgver}.tar.gz"
	],
  	"variables": {
    		"threads" : "4"
  	},
	"build_dependencies": [
      		"cmake"
	],
	"prepare": [
		"tar -xvf package-${pkgver}.tar.gz",
		"cd package-${pkgver}",
		"./configure --prefix=/usr"
	],
	"build": [
		"make -j${threads}"
	],
	"package": [
		"make DESTDIR=${pkgdir} install"
	]
}
```

## Writing a index file
An index file must contain URLS for each package, package version string and package titles. For a working example see the <a href="https://github.com/aidos9/adpm-repo">adpm-repo</a>.

### Example
```
{
	"packages": [
		"package"
	],
	"urls" :{
		"package":"https://package.com/package-v1.0/package.adpm"
	},
	"versions" : {
		"package": "v1.0"
	}
}
```

## To compile adpm
Compile adpm like any other cmake package by doing
```
mkdir build
cd build
cmake ..
make
make install
```

Provided CMake configure options and their defualt values
```
SYSTEM_LIBARCHIVE=ON #Uses the system libarchive if available
WITH_TESTS=ON #Creates the tests
WITH_NETWORK=ON #Allows for the use of repos and remote package files, but requires libcurl
WITH_COLOR=ON #Enables colored output
COMPILE_LIBS=ON #Compiles the library of adpm and then links that to the binaries but the libraries are not installed
```
