#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#define PROGRESSBAR_SIZE 40

class Network
{
public:
    Network();
    bool download_filename(const std::string url, bool check_certs, std::string path);
    bool download(const std::string url, bool check_certs);
    static bool get_filename_from_url(const std::string url, std::string &filename);
    static std::string get_filename_from_url(const std::string url);

private:
    static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream);
    static int progress_func(void *ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded);
};

#endif // NETWORK_H
