#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include <unordered_map>
#include "../libs/json.hpp"

class Config
{
public:
    Config();

    nlohmann::json to_json() const;
    bool from_json(const nlohmann::json &j);

    std::string db_path;
    std::vector<std::string> remote_indexes;
    std::unordered_map<std::string, std::string> variables;
    bool with_color;

    bool operator==(const Config &other);
    bool operator!=(const Config &other);
};

#endif // CONFIG_H
