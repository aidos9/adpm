#ifndef PACKAGEINFO_H
#define PACKAGEINFO_H

#include <string>
#include <vector>
#include "../libs/json.hpp"

class PackageInfo
{
public:
    PackageInfo();

    bool from_json(const nlohmann::json &j);
    nlohmann::json to_json() const;

    std::vector<std::string> files;
    std::string description, version, name;

    bool is_valid();

    bool operator==(const PackageInfo &other);
    bool operator!=(const PackageInfo &other);
};

#endif // PACKAGEINFO_H
