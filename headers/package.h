#ifndef PACKAGE_H
#define PACKAGE_H

#include <string>
#include <vector>
#include <unordered_map>
#include "../libs/json.hpp"

class Package
{
public:
    Package();

    nlohmann::json to_json() const;
    bool from_json(const nlohmann::json &j);

    std::vector<std::string> sources, prepare_commands, build_commands, package_commands, make_dependencies, runtime_dependencies, test_commands, cleanup_files;
    std::unordered_map<std::string, std::string> variables;
    std::string version, name, description;
    bool with_sources_arg = false;
    bool with_variables_arg = false;
    bool with_clean = false;
    bool with_test = false;
    bool with_dependencies_arg = false;
    bool with_build_dependencies_arg = false;
    bool variables_parsed = false;
    bool is_valid();
    bool operator==(const Package &other);
    bool operator!=(const Package &other);
 };

#endif // PACKAGE_H
