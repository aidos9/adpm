#ifndef ADPM_H
#define ADPM_H

#include "color.h"
#include "package.h"
#include "configfile.h"
#include <string>
#include <vector>
#include <unordered_map>

class adpm
{
public:
    adpm();
    int run(int argc, char* argv[]);

private:
    static const Color::Colors operation_color = Color::green;
    static const Color::Colors description_color = Color::cyan;
    int make(int argc, char* argv[]);
    int install(int argc, char* argv[]);
    int test(int argc, char* argv[]);
    int verify(int argc, char* argv[]);
    int clean(int argc, char* argv[]);
    int remove(int argc, char* argv[]);
    int list(int argc, char* argv[]);
    int info(int argc, char* argv[]);
    int help();
    int version();
#ifdef WITH_NETWORK
    int download(int argc, char* argv[]);
    int fetch(int argc, char* argv[]);
    int search(int argc, char* argv[]);
    int update(int argc, char* argv[]);
    bool fetch_func(bool disable_certs);
    bool download_func(bool disable_certs, bool fetch_val, const std::string &name);
#endif
    bool load_config();
    void replace_variables(std::string &line, const Package &pack);
    void make_global(std::vector<std::string> &paths); //Make all the file paths remove the current working directory portion
    void make_paths(std::vector<std::string> &paths);
    void parse_vector_vars(const std::vector<std::string> vars);
    std::string dl_filename;
    std::string pkgdir();
    std::string format_string_colors(std::string str, Color::Colors col, bool bold = false, bool foreground = true);
    std::string root_path;
    std::unordered_map<std::string, std::string> variables = std::unordered_map<std::string, std::string>();
    Config config_file;
};

#endif // ADPM_H
