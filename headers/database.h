#ifndef DATABASE_H
#define DATABASE_H

#include <vector>
#include <string>
#include <unordered_map>
#include "../libs/json.hpp"

class Database
{
public:
    Database();

    bool remove_package(const std::string package);
    bool contains(const std::string package);

    bool from_json(const nlohmann::json &j);
    nlohmann::json to_json() const;

    std::vector<std::string> package_list;
    std::unordered_map<std::string, std::string> package_info, package_version, index_files;
    std::unordered_map<std::string, std::vector<std::string>> package_files;

    bool operator==(const Database &other);
    bool operator!=(const Database &other);
};

#endif // DATABASE_H
