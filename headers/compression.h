#ifndef COMPRESSION_H
#define COMPRESSION_H

#ifdef SYSTEM_LIBARCHIVE
#include <archive.h>
#include <archive_entry.h>
#else
#include "../libs/libarchive/libarchive/archive.h"
#include "../libs/libarchive/libarchive/archive_entry.h"
#endif
#include <string>
#include <vector>

class Compression
{
public:
    Compression();
    static void create(const std::string filename, const std::vector<std::string> files);
    static void extract(const std::string filename);
    static void extract(const std::string filename, const std::string target);

private:
    static int copy_data(struct archive *ar, struct archive *aw);
};

#endif // COMPRESSION_H
