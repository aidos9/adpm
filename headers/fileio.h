#ifndef FILEIO_H
#define FILEIO_H

#include <dirent.h>
#include <string>
#include <vector>
#include "../libs/json.hpp"
#include "package.h"
#include "database.h"
#include "configfile.h"
#include "packageinfo.h"
#ifdef WITH_NETWORK
#include "index.h"
#endif

class FileIO
{
public:
    FileIO();
    static bool write_file(const std::string path, const std::string contents);
    static std::string read_file(const std::string path);
    static bool file_exists(const std::string path);
    static bool dir_exists(const std::string path);
    static bool read_json(const std::string path, nlohmann::json &js);
    static bool write_json(const std::string path,const nlohmann::json &js);
    static bool read_package(const std::string path, Package &package);
    static bool write_package(const std::string path, const Package &pack);
    static bool read_database(const std::string path, Database &db);
    static bool write_database(const std::string path, const Database &db);
#ifdef WITH_NETWORK
    static bool read_index(const std::string path, Index &index);
    static bool write_index(const std::string path, const Index &index);
#endif
    static bool read_pkginfo(const std::string path, PackageInfo &info);
    static bool write_pkginfo(const std::string path, const PackageInfo &info);
    static bool read_config(const std::string path, Config &conf);
    static bool write_config(const std::string path, const Config &conf);
    static bool cwd(std::string& path);
    static bool copy_file(const std::string src, const std::string dest);
    static bool delete_file(const std::string& path);
    bool delete_directory(const std::string& path);
    bool delete_misc(const std::string& path);
    std::vector<std::string> cwd_paths();
    std::vector<std::string> path_list(std::string root_dir);
    std::vector<std::string> path_list_dir(std::string root_dir);
    static bool is_dir(const std::string& path);

private:
    void update_paths();
    std::vector<std::string> paths;
    std::string path;
    void ProcessDir(std::string dir);
    void ProcessEntity(struct dirent *entity);
    void ProcessFile(std::string path);
    void ProcessDir_keep_dir(std::string dir);
    void ProcessEntity_keep_dir(struct dirent *entity);
};

#endif // FILEIO_H
