#ifndef INDEX_H
#define INDEX_H

#include <string>
#include <vector>
#include <unordered_map>
#include "../libs/json.hpp"

class Index
{
public:
    Index();

    bool from_json(const nlohmann::json &j);
    nlohmann::json to_json() const;

    std::vector<std::string> packages;
    std::unordered_map<std::string, std::string> urls, versions;

    bool operator==(const Index &other);
    bool operator!=(const Index &other);
};

#endif // INDEX_H
