#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <vector>
#include <string>

class Algorithms
{
public:
    static int binary_search(const std::vector<std::string> &values, const std::string target);
    static void alpha_order_vector(std::vector<std::string> &values);
    static std::string random_string(int length);
    static std::vector<std::string> values_containing_substr(const std::vector<std::string> values, const std::string &substr);

};

#endif // ALGORITHMS_H
