#ifndef COLOR_H
#define COLOR_H

#include <string>
#include <vector>

class Color
{
public:
    enum Colors{
        red,
        black,
        green,
        yellow,
        blue,
        magenta,
        cyan,
        white
    };
#ifdef WITH_COLOR
    static std::string get_color(Colors col, bool bold = false, bool foreground = true);
    static std::string def();
#endif

private:
#ifdef WITH_COLOR
    static bool supports_colors();
#endif

};

#endif // COLOR_H
